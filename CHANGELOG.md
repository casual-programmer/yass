# Version 2.1.2

Enabled Swift 6 language mode in Package.swift, and fixed the handful of
errors that resulted. Since Yass doesn't contain any concurrent logic itself,
and its model is entirely built on value types, this was trivial. (I just
needed to mark the public model types as Sendable, and change one erroneous
`var` to a `let`.)

# Version 2.1.1

In path strings, numbers are allowed to be in scientific notation, e.g.
"1.0e5" or "1e5" instead of "10000". Yass previously rejected this unless
there was a decimal point there, so it would accept "1.0e5" but not "1e5".
The latter form is less common, but does happen in real SVGs; prior to this
fix, those SVGs would be missing path strings when loaded into Yass.

# Version 2.1.0

Added support for the `display` attribute (CSS and SVG). Many published SVGs
contain hidden elements, perhaps left over from the authoring process, marked
with `display="none"`; this removes them from the rendering. (They're still
visible in the SVG tree, so you can still extract hidden elements by ID, as
intended.)

# Version 2.0.0

New features and a cleaner public API, but with some breaking changes, hence a
new major semver value.

- Linear gradients now work, with some feature exceptions.
- Several other feature gaps were filled, including three-argument rotation,
  viewport size on `<svg>`, `url(#id)` lookups in paint-related attributes,
  percentage values in more places, and more.
- Fail-fast parsing is now controlled by an explicit option to `SVGReader` or
  `yasstool`, and is more consistent. Use this when you want parsing to abort
  immediately on unrecognized/invalid syntax, which is useful when testing on
  new SVG files.
- The presentation attribute cascade, including both parent/child inheritance,
  SVG defaults, and CSS rule handling, is now handled internally and more
  consistently. Instead of being given a struct full of optional presentation
  attributes and a way to apply the cascade yourself, you're now given a
  struct full of non-optional and fully resolved values.
- Updated the minimum swift-tools and OS versions.

# Version 1.4.2

The `Yass` library product is now declared as automatic. Previously, it
explicitly specified dynamic linkage, due to a bug in Xcode which had
prevented me from linking it into both an app and an app extension. That bug
was fixed a while ago, however. (I left separate YassStatic and YassDynamic
targets as well, but don't expect them to be used.)

# Version 1.4.1

(Just *minutes* after 1.4.0...)

I'd pushed 1.4.0 after only test-building for a macOS target. So naturally it
referenced a symbol which did not exist on iOS. (`CGColor.black`). Fixed.

# Version 1.4.0

Color support:
- Added `rebeccapurple`.
- Implemented `currentColor`, which in turn requires the `color` attribute.

# Version 1.3.0

The main change in this version is support for very simple embedded CSS
stylesheets (the `<style>` element). A stylesheet may only use class or ID
selectors, and the properties they configure are the same as were previously
supported in `style=""` inline style attributes. This was driven by finding
some commonly-used icons which relied on `<style>` elements.

Smaller user-visible or API-visible changes:
- Explicit `matrix(a,b,c,d,tx,ty)` transforms are now supported
- The `CGAffineTransform` extension methods are now public

Internally, I reorganized the code a bit, separating the model, syntax, and
Core Graphics layers into directories, and separating the model types between
structural and presentational.

(I did not keep a proper changelog prior to this version.)

