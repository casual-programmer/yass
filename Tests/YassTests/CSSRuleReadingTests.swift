//
//    CSSRuleReadingTests.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import XCTest
@testable import Yass

final class CSSRuleReadingTests: XCTestCase {

    private struct ResolvedCSSRule: Equatable {
        var selector: CSSSelector
        var presentationAttributes: SVGPresentationAttributeSet
    }

    private func readRules(_ str: String) -> [ResolvedCSSRule] {
        guard let rules = try? CSSReader().readRules(str, strict: false) else {
            return []
        }
        return rules.map { ResolvedCSSRule(selector: $0.selector, presentationAttributes: $0.presentationAttributes) }
    }

    func testEmpty() {
        XCTAssertEqual([], readRules(""))
        XCTAssertEqual([], readRules("/* nothing to do here */"))
        XCTAssertEqual([], readRules("  "))
        XCTAssertEqual([], readRules("  /* a */ /* b */ "))
    }

    func testOneEmptyRule() {
        let empty = SVGPresentationAttributeSet()

        XCTAssertEqual([ResolvedCSSRule(selector: .class("cls"), presentationAttributes: empty)],
                       readRules(".cls{}"))
        XCTAssertEqual([ResolvedCSSRule(selector: .class("cls"), presentationAttributes: empty)],
                       readRules(" /*this is a rule:*/ .cls { /* this would be the properties */ } "))

        XCTAssertEqual([ResolvedCSSRule(selector: .id("id"), presentationAttributes: empty)],
                       readRules("#id{}"))
        XCTAssertEqual([ResolvedCSSRule(selector: .id("id"), presentationAttributes: empty)],
                       readRules(" /*this is a rule:*/ #id { /* this would be the properties */ } "))
    }

    func testABitMore() throws {
        let css = """
        .class1 {
            display: none;
            unknown-attribute: gets ignored;
            fill: #ffffff;
            fill-rule: nonzero;
            fill-opacity: 0.5;
            stroke: #000000;
            stroke-opacity: 0.75;
            stroke-width: 2;
            stroke-linecap: round;
            stroke-linejoin: arcs;
            stroke-miterlimit: 0.1;
            stroke-dasharray: 1 2 3;
            stroke-dashoffset: 2;
        }
        """

        let rules = readRules(css)

        let paset = try XCTUnwrap(rules.first { $0.selector == .class("class1") }).presentationAttributes
        let pa = SVGDrawingAttributes(paset, using: SVGLookup())

        let expectedPA = SVGDrawingAttributes(
            display: .none,
            fill: .rgba(1, 1, 1, 1),
            fillRule: .nonzero,
            fillOpacity: 0.5,
            stroke: .rgba(0, 0, 0, 1),
            strokeOpacity: 0.75,
            strokeWidth: 2,
            strokeLineCap: .round,
            strokeLineJoin: .arcs,
            strokeMiterLimit: 0.1,
            strokeDashArray: [1, 2, 3],
            strokeDashOffset: 2,
            transform: [],
            currentColor: .black
        )

        XCTAssertEqual(expectedPA, pa)
    }

    func testWithLookups() throws {
        let css = """
        .class1 {
            fill: url(#f1);
        }
        """

        // Normally this will be a gradient or something, not a plain color,
        // but the framework doesn't care
        var lookup = SVGLookup()
        lookup.register(id: "f1", .fixed(SVGPaint.rgba(1, 0, 1, 1)))

        let rules = try CSSReader().readRules(css, strict: true)
        let attrs = try XCTUnwrap(rules.first).presentationAttributes

        XCTAssertEqual(
            attrs.get(SVGPAs.Fill.self, using: lookup),
            SVGPaint.rgba(1, 0, 1, 1)
        )
    }
}
