//
//    PresentationAttributeTests.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import XCTest
@testable import Yass

final class PresentationAttributeTests: XCTestCase {

    // MARK: - Basic CSS property-list parsing

    func testEmptyCSS() {
        XCTAssertEqual([:], CSSReader().readProperties(""))
        XCTAssertEqual([:], CSSReader().readProperties("/* nothing to do here */"))
        XCTAssertEqual([:], CSSReader().readProperties("  "))
        XCTAssertEqual([:], CSSReader().readProperties(";"))
        XCTAssertEqual([:], CSSReader().readProperties(";;; /* comment */  ;;;"))
    }

    func testBasicCSS() {
        XCTAssertEqual(["fill": "#ffffff"], CSSReader().readProperties("fill:#ffffff"))
        XCTAssertEqual(["fill": "#ffffff"], CSSReader().readProperties("fill:#ffffff;"))
        XCTAssertEqual(["fill": "#ffffff"], CSSReader().readProperties("fill: #ffffff"))
        XCTAssertEqual(["fill": "#ffffff"], CSSReader().readProperties(";  fill   : /*white*/ #ffffff  ;"))
    }

    func testMultipleProperties() {
        XCTAssertEqual(
            ["fill": "#ffffff", "stroke": "#000000"],
            CSSReader().readProperties("fill: #ffffff; stroke: #000000")
        )
        XCTAssertEqual(
            ["fill": "#ffffff", "stroke": "#000000"],
            CSSReader().readProperties("/* use a white fill and black stroke */; fill: #ffffff; stroke: #000000;")
        )
    }


    // MARK: - Application of inline `style` attributes

    func testInlineCSS() throws {
        let xml = """
        <?xml version="1.0"?>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200">
          <circle style="fill: #ffffff" fill="#cccccc" stroke="#000000" cx="100" cy="100" r="100"/>
        </svg>
        """.data(using: .utf8)!

        let el = try SVGReader().read(data: xml)

        guard case .svg(_, _, _, let elements) = el else {
            XCTFail()
            return
        }

        guard let circle = elements.first, case .graphic(_, let pres, .circle) = circle else {
            XCTFail()
            return
        }

        XCTAssertEqual(SVGPaint.rgba(1, 1, 1, 1), pres.fill)    // style attribute overrides direct attributes
        XCTAssertEqual(SVGPaint.rgba(0, 0, 0, 1), pres.stroke)
    }


    // MARK: - Tests of identified/referenced gradient objects

    func testLinearGradientByID() throws {
        let xml = """
        <?xml version="1.0"?>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200">
          <linearGradient gradientUnits="userSpaceOnUse" id="lg1" x1="0" y1="0%" x2="200" y2="100%">
            <stop offset="0" stop-color="#000000"/>
            <stop offset="1" stop-color="#FFFFFF" stop-opacity="50%"/>
          </linearGradient>
          <circle fill="url(#lg1)" cx="100" cy="100" r="100"/>
        </svg>
        """.data(using: .utf8)!

        let el = try SVGReader().read(data: xml)

        guard case .svg(_, _, _, let elements) = el else {
            XCTFail()
            return
        }

        guard let circle = elements.first, case .graphic(_, let pres, .circle) = circle else {
            XCTFail()
            return
        }

        let lg1 = SVGPaint.linearGradient(
            units: .userSpaceOnUse,
            transform: [],
            spread: .pad,
            x1: .distance(0),
            y1: .fraction(0),
            x2: .distance(200),
            y2: .fraction(1),
            stops: [
                SVGGradientStop(offset: 0, color: .rgba(0, 0, 0, 1), opacity: 1),
                SVGGradientStop(offset: 1, color: .rgba(1, 1, 1, 1), opacity: 0.5),
            ]
        )

        XCTAssertEqual(lg1, pres.fill)    // style attribute overrides direct attributes
    }

    // linearGradient doesn't need to live within a <defs>, but it can
    func testLinearGradientByIDInDefs() throws {
        let xml = """
        <?xml version="1.0"?>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200">
          <defs>
            <linearGradient gradientUnits="userSpaceOnUse" id="lg1" x1="0" y1="0%" x2="200" y2="100%">
              <stop offset="0" stop-color="#000000"/>
              <stop offset="1" stop-color="#FFFFFF"/>
            </linearGradient>
          </defs>
          <circle fill="url(#lg1)" cx="100" cy="100" r="100"/>
        </svg>
        """.data(using: .utf8)!

        let el = try SVGReader().read(data: xml)

        guard case .svg(_, _, _, let elements) = el else {
            XCTFail()
            return
        }

        guard let circle = elements.first, case .graphic(_, let pres, .circle) = circle else {
            XCTFail()
            return
        }

        let lg1 = SVGPaint.linearGradient(
            units: .userSpaceOnUse,
            transform: [],
            spread: .pad,
            x1: .distance(0),
            y1: .fraction(0),
            x2: .distance(200),
            y2: .fraction(1),
            stops: [
                SVGGradientStop(offset: 0, color: .rgba(0, 0, 0, 1), opacity: 1),
                SVGGradientStop(offset: 1, color: .rgba(1, 1, 1, 1), opacity: 1),
            ]
        )

        XCTAssertEqual(lg1, pres.fill)
    }

    func testLinearGradientIDMismatch() throws {
        let xml = """
        <?xml version="1.0"?>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200">
          <defs>
            <linearGradient gradientUnits="userSpaceOnUse" id="lg1" x1="0" y1="0" x2="200" y2="200">
              <stop offset="0" stop-color="#000000"/>
              <stop offset="1" stop-color="#FFFFFF"/>
            </linearGradient>
          </defs>
          <circle fill="url(#lg2)" cx="100" cy="100" r="100"/>
        </svg>
        """.data(using: .utf8)!

        let el = try SVGReader().read(data: xml)

        guard case .svg(_, _, _, let elements) = el else {
            XCTFail()
            return
        }

        guard let circle = elements.first, case .graphic(_, let pres, .circle) = circle else {
            XCTFail()
            return
        }

        // unknown reference -> black fill
        XCTAssertEqual(SVGPaint.black, pres.fill)
    }
}
