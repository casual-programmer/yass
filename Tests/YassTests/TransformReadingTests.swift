//
//    TransformReadingTests.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import XCTest
@testable import Yass

final class TransformReadingTests: XCTestCase {

    func testScale() throws {
        XCTAssertEqual(
            try SVGTransformReader().read("scale(2)"),
            [.scale(2.0, 2.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("scale(2.0)"),
            [.scale(2.0, 2.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("scaleX(2.0)"),
            [.scale(2.0, 1.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("scaleY(2.0)"),
            [.scale(1.0, 2.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("scale(0.001, 1000.0)"),
            [.scale(0.001, 1000.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("scale(0.001 1000.0)"),
            [.scale(0.001, 1000.0)]
        )
    }

    func testBadScale() {
        XCTAssertThrowsError(try SVGTransformReader().read("scale()"))
        XCTAssertThrowsError(try SVGTransformReader().read("scaleX()"))
        XCTAssertThrowsError(try SVGTransformReader().read("scaleY()"))
        XCTAssertThrowsError(try SVGTransformReader().read("scale(,)"))
        XCTAssertThrowsError(try SVGTransformReader().read("scale(large)"))
        XCTAssertThrowsError(try SVGTransformReader().read("scale(1,2,3)"))
    }

    func testTranslate() {
        XCTAssertEqual(
            try SVGTransformReader().read("translate(2)"),
            [.translate(2.0, 0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("translateX(2.0)"),
            [.translate(2.0, 0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("translateY(2.0)"),
            [.translate(0, 2.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("translate(0.001, 1000.0)"),
            [.translate(0.001, 1000.0)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("translate(0.001 1000.0)"),
            [.translate(0.001, 1000.0)]
        )
    }

    func testBadTranslate() {
        XCTAssertThrowsError(try SVGTransformReader().read("translate()"))
        XCTAssertThrowsError(try SVGTransformReader().read("translateX()"))
        XCTAssertThrowsError(try SVGTransformReader().read("translateY()"))
        XCTAssertThrowsError(try SVGTransformReader().read("translate(,)"))
        XCTAssertThrowsError(try SVGTransformReader().read("translate(large)"))
        XCTAssertThrowsError(try SVGTransformReader().read("translate(1,2,3)"))
        XCTAssertThrowsError(try SVGTransformReader().read("translate(1.2.3)"))
    }

    func testRotate() throws {
        XCTAssertEqual(
            try SVGTransformReader().read("rotate(180)"),
            [.rotate(180, around: .zero)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(180 20 30)"),
            [.rotate(180, around: CGPoint(x: 20, y: 30))]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(180deg)"),
            [.rotate(180, around: .zero)]
        )

        // may need to become an approximate equality check
        XCTAssertEqual(
            try SVGTransformReader().read("rotate(\(CGFloat.pi)rad)"),
            [.rotate(180, around: .zero)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(200grad)"),
            [.rotate(180, around: .zero)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(0.5turn)"),
            [.rotate(180, around: .zero)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(0.5turn 20 30)"),
            [.rotate(180, around: CGPoint(x: 20, y: 30))]
        )
    }

    func testBadRotate() {
        XCTAssertThrowsError(try SVGTransformReader().read("rotate()"))
        XCTAssertThrowsError(try SVGTransformReader().read("rotate(180badunit)"))
        XCTAssertThrowsError(try SVGTransformReader().read("rotate(deg)"))
        XCTAssertThrowsError(try SVGTransformReader().read("rotate(1,1)"))
        XCTAssertThrowsError(try SVGTransformReader().read("rotate(1 1)"))
        XCTAssertThrowsError(try SVGTransformReader().read("rotate(1 2 3deg)"))
        XCTAssertThrowsError(try SVGTransformReader().read("rotate(1 2deg 3)"))
    }

    func testSequencing() throws {
        XCTAssertEqual(
            try SVGTransformReader().read("rotate(180deg) translate(1,2) scale(3,4)"),
            [.rotate(180, around: .zero), .translate(1, 2), .scale(3, 4)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(180deg)translate(1,2)scale(3,4)"),
            [.rotate(180, around: .zero), .translate(1, 2), .scale(3, 4)]
        )

        XCTAssertEqual(
            try SVGTransformReader().read("rotate(180deg), translate(1,2), scale(3,4)"),
            [.rotate(180, around: .zero), .translate(1, 2), .scale(3, 4)]
        )
    }
}

