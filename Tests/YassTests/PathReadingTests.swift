//
//    PathReadingTests.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import XCTest
@testable import Yass

fileprivate func P(_ x: CGFloat, _ y: CGFloat) -> CGPoint {
    CGPoint(x: x, y: y)
}

final class PathReadingTests: XCTestCase {

    func testNumberLexing() throws {
        // positive, nonscientific
        do {
            var lexer = SVGPathLexer("1")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1, n)
        }
        do {
            var lexer = SVGPathLexer("1.")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1, n)
        }
        do {
            var lexer = SVGPathLexer(".5")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(0.5, n)
        }
        do {
            var lexer = SVGPathLexer("1.5")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1.5, n)
        }

        // explicitly positive, nonscientific
        do {
            var lexer = SVGPathLexer("+1")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1, n)
        }
        do {
            var lexer = SVGPathLexer("+1.")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1, n)
        }
        do {
            var lexer = SVGPathLexer("+.5")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(0.5, n)
        }
        do {
            var lexer = SVGPathLexer("+1.5")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1.5, n)
        }

        // positive, scientific
        do {
            var lexer = SVGPathLexer("1e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1e10, n)
        }
        do {
            var lexer = SVGPathLexer("1.e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1e10, n)
        }
        do {
            var lexer = SVGPathLexer(".5e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(0.5e10, n)
        }
        do {
            var lexer = SVGPathLexer("1.5e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1.5e10, n)
        }

        // positive, explicitly positive scientific
        do {
            var lexer = SVGPathLexer("1e+10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1e10, n)
        }
        do {
            var lexer = SVGPathLexer("1.e+10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1e10, n)
        }
        do {
            var lexer = SVGPathLexer(".5e+10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(0.5e10, n)
        }
        do {
            var lexer = SVGPathLexer("1.5e+10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1.5e10, n)
        }

        // explicitly positive, scientific
        do {
            var lexer = SVGPathLexer("+1e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1e10, n)
        }
        do {
            var lexer = SVGPathLexer("+1.e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1e10, n)
        }
        do {
            var lexer = SVGPathLexer("+.5e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(0.5e10, n)
        }
        do {
            var lexer = SVGPathLexer("+1.5e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(1.5e10, n)
        }

        // negative, nonscientific
        do {
            var lexer = SVGPathLexer("-1")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-1, n)
        }
        do {
            var lexer = SVGPathLexer("-1.")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-1, n)
        }
        do {
            var lexer = SVGPathLexer("-.5")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-0.5, n)
        }
        do {
            var lexer = SVGPathLexer("-1.5")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-1.5, n)
        }

        // negative, scientific
        do {
            var lexer = SVGPathLexer("-1e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-1e10, n)
        }
        do {
            var lexer = SVGPathLexer("-1.e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-1e10, n)
        }
        do {
            var lexer = SVGPathLexer("-.5e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-0.5e10, n)
        }
        do {
            var lexer = SVGPathLexer("-1.5e10")
            let (n, _) = try XCTUnwrap(try lexer.scanNumber())
            XCTAssertEqual(-1.5e10, n)
        }
    }

    func testMoveTo() throws {
        XCTAssertEqual(
            try SVGPathReader().read("M 5 5 M 10 10 m 5 5"),
            [
                .moveTo(CGPoint(x: 5, y: 5)),
                .moveTo(CGPoint(x: 10, y: 10)),
                .moveTo(CGPoint(x: 15, y: 15))
            ]
        )

        XCTAssertEqual(
            try SVGPathReader().read("M 5, 5 M 10, 10 m 5, 5"),
            [
                .moveTo(CGPoint(x: 5, y: 5)),
                .moveTo(CGPoint(x: 10, y: 10)),
                .moveTo(CGPoint(x: 15, y: 15))
            ]
        )

        XCTAssertEqual(
            try SVGPathReader().read("M5-5M.10-10M5.0e5-5"),
            [
                .moveTo(CGPoint(x: 5, y: -5)),
                .moveTo(CGPoint(x: 0.10, y: -10)),
                .moveTo(CGPoint(x: 500000, y: -5))
            ]
        )

        // initial 'm' is effectively absolute
        XCTAssertEqual(
            try SVGPathReader().read("m 5 5 M 10 10 m 5 5"),
            [
                .moveTo(CGPoint(x: 5, y: 5)),
                .moveTo(CGPoint(x: 10, y: 10)),
                .moveTo(CGPoint(x: 15, y: 15))
            ]
        )
    }

    func testMoveToThenImplicitLines() throws {
        XCTAssertEqual(
            try SVGPathReader().read("M 5 5 10 10 15 15"),
            [
                .moveTo(CGPoint(x: 5, y: 5)),
                .lineTo(CGPoint(x: 10, y: 10)),
                .lineTo(CGPoint(x: 15, y: 15))
            ]
        )

        // initial 'm' is effectively absolute, but it still makes implicit lines relative
        XCTAssertEqual(
            try SVGPathReader().read("m 5 5 10 10 15 15"),
            [
                .moveTo(CGPoint(x: 5, y: 5)),
                .lineTo(CGPoint(x: 15, y: 15)),
                .lineTo(CGPoint(x: 30, y: 30))
            ]
        )
    }

    func testQuadratics() throws {
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  Q 1,1 2,0  Q 3,-1 4,0"),
            [
                .moveTo(P(0, 0)),
                .quadCurveTo(P(2, 0), control: P(1, 1)),
                .quadCurveTo(P(4, 0), control: P(3, -1)),
            ]
        )

        // same, but using repeated args
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  Q 1,1 2,0  3,-1 4,0"),
            [
                .moveTo(P(0, 0)),
                .quadCurveTo(P(2, 0), control: P(1, 1)),
                .quadCurveTo(P(4, 0), control: P(3, -1)),
            ]
        )

        // same, but using T to omit the first control point
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  Q 1,1 2,0  T 4,0"),
            [
                .moveTo(P(0, 0)),
                .quadCurveTo(P(2, 0), control: P(1, 1)),
                .quadCurveTo(P(4, 0), control: P(3, -1)),
            ]
        )

        // same, but using repeated args and relative coords
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  q 1,1 2,0  1,-1 2,0"),
            [
                .moveTo(P(0, 0)),
                .quadCurveTo(P(2, 0), control: P(1, 1)),
                .quadCurveTo(P(4, 0), control: P(3, -1)),
            ]
        )

        // same, but using T and relative coords
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  q 1,1 2,0  t 2,0"),
            [
                .moveTo(P(0, 0)),
                .quadCurveTo(P(2, 0), control: P(1, 1)),
                .quadCurveTo(P(4, 0), control: P(3, -1)),
            ]
        )
    }

    func testCubics() throws {
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  C 1,1 3,-1 4,0  C 5,1 7,-1 8,0"),
            [
                .moveTo(P(0, 0)),
                .cubicCurveTo(P(4, 0), control1: P(1,1), control2: P(3,-1)),
                .cubicCurveTo(P(8, 0), control1: P(5,1), control2: P(7,-1))
            ]
        )

        // same, but using repeated args for same command
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  C 1,1 3,-1 4,0  5,1 7,-1 8,0"),
            [
                .moveTo(P(0, 0)),
                .cubicCurveTo(P(4, 0), control1: P(1,1), control2: P(3,-1)),
                .cubicCurveTo(P(8, 0), control1: P(5,1), control2: P(7,-1))
            ]
        )

        // same, but using S to omit the first control point
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  C 1,1 3,-1 4,0  S 7,-1 8,0"),
            [
                .moveTo(P(0, 0)),
                .cubicCurveTo(P(4, 0), control1: P(1,1), control2: P(3,-1)),
                .cubicCurveTo(P(8, 0), control1: P(5,1), control2: P(7,-1))
            ]
        )

        // same, but using repeated args and relative coordinates
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  c 1,1 3,-1 4,0  1,1 3,-1 4,0"),
            [
                .moveTo(P(0, 0)),
                .cubicCurveTo(P(4, 0), control1: P(1,1), control2: P(3,-1)),
                .cubicCurveTo(P(8, 0), control1: P(5,1), control2: P(7,-1))
            ]
        )

        // same, but using S and relative coordinates
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  c 1,1 3,-1 4,0  s 3,-1 4,0"),
            [
                .moveTo(P(0, 0)),
                .cubicCurveTo(P(4, 0), control1: P(1,1), control2: P(3,-1)),
                .cubicCurveTo(P(8, 0), control1: P(5,1), control2: P(7,-1))
            ]
        )
    }

    func testArcs() throws {
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  A 10,20 45 1 0 12,12"),
            [
                .moveTo(P(0, 0)),
                .arcTo(P(12, 12), radiusX: 10, radiusY: 20, rotation: 45, largeArc: true, sweep: false)
            ]
        )

        // same, but omitting space between and around the flags, which, as bad
        // as it looks, is not only legal, but included in the w3c official test suite
        XCTAssertEqual(
            try SVGPathReader().read("M 0,0  A 10,20 45 1012,12"),
            [
                .moveTo(P(0, 0)),
                .arcTo(P(12, 12), radiusX: 10, radiusY: 20, rotation: 45, largeArc: true, sweep: false)
            ]
        )
    }
}
