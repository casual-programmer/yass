//
//    ColorReadingTests.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import XCTest
@testable import Yass

final class ColorReadingTests: XCTestCase {

    func testHex() {
        let x88 = CGFloat(0x88)/CGFloat(0xff)

        XCTAssertEqual(
            SVGPaintReader().read("#888"),
            SVGPaint.rgba(x88, x88, x88, 1.0)
        )

        XCTAssertEqual(
            SVGPaintReader().read("#fff"),
            SVGPaint.rgba(1.0, 1.0, 1.0, 1.0)
        )

        XCTAssertEqual(
            SVGPaintReader().read("#888888"),
            SVGPaint.rgba(x88, x88, x88, 1.0)
        )

        XCTAssertEqual(
            SVGPaintReader().read("#000"),
            SVGPaint.rgba(0, 0, 0, 1)
        )

        XCTAssertEqual(
            SVGPaintReader().read("#000000"),
            SVGPaint.rgba(0, 0, 0, 1)
        )

        // alpha is not allowed in hex per CSS spec, but I allow it
        XCTAssertEqual(
            SVGPaintReader().read("#88888888"),
            SVGPaint.rgba(x88, x88, x88, x88)
        )

        XCTAssertEqual(
            SVGPaintReader().read("#00000000"),
            SVGPaint.rgba(0, 0, 0, 0)
        )
    }

    func testBad() {
        XCTAssertNil(SVGPaintReader().read(""))
        XCTAssertNil(SVGPaintReader().read("#"))
        XCTAssertNil(SVGPaintReader().read("ffffff"))
        XCTAssertNil(SVGPaintReader().read("#fffffffffffff"))
        XCTAssertNil(SVGPaintReader().read("unknowncolorname"))
    }
}
