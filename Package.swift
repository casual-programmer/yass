// swift-tools-version: 6.0

import PackageDescription

let package = Package(
    name: "Yass",
    platforms: [
        .macOS(.v10_15),
        .iOS(.v13)
    ],
    products: [
        .executable(
            name: "yasstool",
            targets: ["yasstool"]
        ),
        .library(
            name: "Yass",
            targets: ["Yass"]
        ),
    ],
    dependencies: [
        .package(url: "https://github.com/apple/swift-argument-parser", from: "1.0.0")
    ],
    targets: [
        .executableTarget(
            name: "yasstool",
            dependencies: [
                "Yass",
                .product(name: "ArgumentParser", package: "swift-argument-parser")
            ]
        ),
        .target(
            name: "Yass",
            dependencies: []
        ),
        .testTarget(
            name: "YassTests",
            dependencies: ["Yass"]
        ),
    ]
)
