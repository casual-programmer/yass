
# Yass (Yet Another Swift SVG [library])

Yass is a pure-Swift package for loading and rendering SVGs.

It's small, self-contained, and easy to use. It supports a useful subset of
[SVG][svg], works on both macOS and iOS, is pretty fast, comes with a simple
command-line tool, and is deployed as part of a shipping Mac app today.

The primary goal of Yass is to render symbols and icons. The current version
successfully handles SVGs from many commonly-used symbol libraries, such as
[FontAwesome Free][fasvg], and produced by common drawing tools. It's been 
growing in capabilities with each version, but still has somewhat limited
CSS support, and no support for text, animation, scripting, etc. I tend to
work on it whenever I find an icon I want to use which doesn't draw right;
feel free to reach out if you see something you'd like me to look into.

## Usage

To parse an SVG fragment or element:

    import Yass
    let svg = try SVGReader().read(data: /* your Data object */)

Here, `svg` will have type `SVGElement`, which is a (recursive) Swift enum.
Yass's data model was designed entirely around Swift value types, using enums
for things like elements, shapes, and path instructions, and structs to bundle
up drawing attributes. This makes it simple to pattern-match the parsed SVG
data with `switch` statements, and lets you safely pass data between threads.

(This is in opposition to most other SVG toolkits, which, borrowing from the
W3C's official DOM, present an object- or protocol-oriented interface.)

Extension methods are added to CGContext and CGMutablePath to easily apply SVG
objects in your drawing. (Every extension on a system type is prefixed with
`svg_`, to avoid conflicting with current or future names.) For example:

    let context = /* your CGContext */
    let rect = /* your destination CGRect */
    context.svg_draw(svg, in: rect)

These are just the highest-level interfaces; there is also `SVGPathReader` to
process a standalone SVG path string (the `d` attribute), and there are other
methods on the CGContext extension to allow adding SVG element paths without
rendering them or apply SVG attributes to an existing path.

## `yasstool`

This package also defines a target called `yasstool`; this is a command-line
Mac tool which can be used to render an SVG to PNG, or just to parse it and
dump its structure. (I built it for testing. I have some shell scripts which
run it against a bunch of third-party SVGs for validation.)

## Primary application

I developed Yass to allow [TimeStory][timestory], my macOS (AppKit)
timeline-drawing app, to render "point event shapes", small symbols or icons
placed on a timeline to mark a point in time. Check it out!

## Copyright & License

Yass is (c) 2020-2023 [Aaron Trickey][me], and has been released under the
terms of the MIT license. Feel free to use it in your apps, whether open or
closed source. If you use it in anything, I'd love to hear about it.

 [svg]: https://www.w3.org/TR/SVG/
 [timestory]: https://timestory.app/
 [me]: https://casualprogrammer.com/
 [fasvg]: https://github.com/FortAwesome/Font-Awesome/tree/master/svgs

