//
//    Yasstool.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2021-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation
import Yass
import ArgumentParser
import CoreGraphics
import ImageIO
import CoreServices


@main
struct Yasstool: ParsableCommand {
    @Argument(help: "An input SVG file")
    var svgFile: String

    @Flag(help: "Prefix output with the input file name (useful in scripts)")
    var filename: Bool = false

    @Flag(help: "Dump the parsed SVGElement data structure")
    var dump: Bool = false

    @Flag(help: "Print out the bounding-box size for the given SVG, in its own coordinate system")
    var size: Bool = false

    @Option(help: "Render to a PNG at the given location")
    var render: String?

    @Flag(name: [.customShort("x"), .long],
          help: "Fail with an error message on any unrecognized syntax")
    var failFast: Bool = false

    func run() throws {
        let url = URL(fileURLWithPath: svgFile)
        let data = try Data(contentsOf: url)

        let svg: SVGElement = try SVGReader().read(data: data, failFast: failFast)

        var boxSize: CGSize = .zero
        if size || render != nil {
            if case .svg(_, let fa, _, _) = svg, let viewBox = fa.viewBox {
                boxSize = viewBox.size
            } else {
                let path = CGMutablePath()
                path.svg_add(svg)
                boxSize = path.boundingBoxOfPath.size
            }
        }

        if filename {
            print("\(svgFile): ", terminator: "")
        }

        if size {
            print("Size: \(boxSize.width)✕\(boxSize.height)")
        }

        if dump {
            Swift.dump(svg)
        }

        if let render = render {
            guard let ctx = CGContext(
                data: nil,
                width: Int(ceil(boxSize.width)),
                height: Int(ceil(boxSize.height)),
                bitsPerComponent: 8,
                bytesPerRow: 0,
                space: CGColorSpace(name: CGColorSpace.sRGB)!,
                bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue
            ) else {
                throw RuntimeError.failedToAllocateBitmap
            }

            ctx.scaleBy(x: 1, y: -1)
            ctx.translateBy(x: 0, y: -ceil(boxSize.height))

            ctx.svg_draw(svg, in: CGRect(origin: .zero, size: boxSize))

            guard let img = ctx.makeImage() else {
                throw RuntimeError.failedToAcquireBitmap
            }

            let url = URL(fileURLWithPath: render)
            guard let dest = CGImageDestinationCreateWithURL(url as CFURL, kUTTypePNG, 1, nil) else {
                throw RuntimeError.failedToCreateImageFile
            }

            CGImageDestinationAddImage(dest, img, nil)
            guard CGImageDestinationFinalize(dest) else {
                throw RuntimeError.failedToFinalizeImageFile
            }

            print("Wrote to \(render)")
        }

        if filename, !dump, !size, render == nil {
            print() // newline
        }
    }

    enum RuntimeError: Error {
        case failedToAllocateBitmap
        case failedToAcquireBitmap
        case failedToCreateImageFile
        case failedToFinalizeImageFile
    }
}
