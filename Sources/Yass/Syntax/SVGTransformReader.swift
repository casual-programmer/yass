//
//    SVGTransformReader.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation
import CoreGraphics


public struct SVGTransformSyntaxError: LocalizedError {
    var pathString: String
    var characterIndex: String.Index

    public var errorDescription: String? {
        var marked = pathString
        marked.insert("➤", at: characterIndex)
        return "syntax error in SVG/CSS transform syntax (marked with ➤): \"\(marked)\""
    }
}


struct SVGTransformReader {
    /// Reads a sequence of transforms in SVG syntax, which overlaps with but
    /// is not identical to CSS transform syntax.
    ///
    /// Notes:
    /// - Commas are treated as whitespace and never required. Both forms exist in the
    ///   wild, although the spec calls for commas between arguments and whitespace between
    ///   transform functions.
    /// - Linear units and percentages are unsupported.
    /// - See `SVGTransform` for the specific transforms allowed; this parser
    ///   takes care of variant spellings, like `translateX` vs. `translate`.
    ///
    public func read(_ string: String) throws -> [SVGTransform] {
        let tokens = try lex(string)
        var transforms: [SVGTransform] = []
        var iter = tokens.makeIterator()
        while let token = iter.next() {
            guard case .name(let name, at: let start) = token else {
                throw SVGTransformSyntaxError(pathString: string, characterIndex: token.startIndex)
            }

            switch name {
            case "translate":
                guard
                    case .openParen = iter.next(),
                    case .number(let dx, at: _)? = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }

                switch iter.next() {
                case .number(let dy, at: _):
                    guard
                        case .closeParen = iter.next()
                        else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }
                    transforms.append(.translate(dx, dy))

                case .closeParen:
                    transforms.append(.translate(dx, 0))

                default:
                    throw SVGTransformSyntaxError(pathString: string, characterIndex: start)
                }

            case "translateX":
                guard
                    case .openParen = iter.next(),
                    case .number(let dx, at: _) = iter.next(),
                    case .closeParen = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }
                transforms.append(.translate(dx, 0))

            case "translateY":
                guard
                    case .openParen = iter.next(),
                    case .number(let dy, at: _) = iter.next(),
                    case .closeParen = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }
                transforms.append(.translate(0, dy))

            case "scale":
                guard
                    case .openParen = iter.next(),
                    case .number(let sx, at: _) = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }

                switch iter.next() {
                case .number(let sy, at: _):
                    guard
                        case .closeParen = iter.next()
                        else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }
                    transforms.append(.scale(sx, sy))

                case .closeParen:
                    transforms.append(.scale(sx, sx))

                default:
                    throw SVGTransformSyntaxError(pathString: string, characterIndex: start)
                }

            case "scaleX":
                guard
                    case .openParen = iter.next(),
                    case .number(let sx, at: _) = iter.next(),
                    case .closeParen = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }
                transforms.append(.scale(sx, 1))

            case "scaleY":
                guard
                    case .openParen = iter.next(),
                    case .number(let sy, at: _) = iter.next(),
                    case .closeParen = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }
                transforms.append(.scale(1, sy))

            case "rotate":
                guard
                    case .openParen = iter.next(),
                    case .number(var degrees, at: _) = iter.next()
                    else { throw SVGTransformSyntaxError(pathString: string, characterIndex: start) }

                var beyondUnit = false
                var origin: CGPoint = .zero

            scanloop:
                while let nextToken = iter.next() {
                    switch nextToken {
                    case .closeParen(at: _):
                        transforms.append(.rotate(degrees, around: origin))
                        break scanloop

                    case .name(let unit, at: let index) where !beyondUnit:
                        beyondUnit = true
                        switch unit {
                        case "deg":     break
                        case "rad":     degrees = degrees * 180.0 / .pi
                        case "grad":    degrees = degrees * 360.0 / 400.0
                        case "turn":    degrees = degrees * 360.0
                        default:        throw SVGTransformSyntaxError(pathString: string, characterIndex: index)
                        }

                    case .number(let x, at: let index):
                        beyondUnit = true
                        guard case .number(let y, at: _) = iter.next() else {
                            throw SVGTransformSyntaxError(pathString: string, characterIndex: index)
                        }
                        origin = CGPoint(x: x, y: y)

                    default:
                        throw SVGTransformSyntaxError(pathString: string, characterIndex: start)
                    }
                }

            case "matrix":
                guard
                    case .openParen = iter.next(),
                    case .number(let a, at: _) = iter.next(),
                    case .number(let b, at: _) = iter.next(),
                    case .number(let c, at: _) = iter.next(),
                    case .number(let d, at: _) = iter.next(),
                    case .number(let tx, at: _) = iter.next(),
                    case .number(let ty, at: _) = iter.next(),
                    case .closeParen = iter.next()
                else {
                    throw SVGTransformSyntaxError(pathString: string, characterIndex: start)
                }

                transforms.append(.matrix(a, b, c, d, tx, ty))

            default:
                throw SVGTransformSyntaxError(pathString: string, characterIndex: start)
            }
        }
        return transforms
    }


    private enum Token {
        case name(String, at: String.Index)
        case openParen(at: String.Index)
        case closeParen(at: String.Index)
        case number(CGFloat, at: String.Index)

        var startIndex: String.Index {
            switch self {
            case .name(_, at: let i),
                 .openParen(at: let i),
                 .closeParen(at: let i),
                 .number(_, at: let i):
                return i
            }
        }
    }


    /// Scans a transform string into tokens. See `read(_:)` for more details.
    ///
    private func lex(_ string: String) throws -> [Token] {
        var tokens: [Token] = []
        var state = LexicalState.start

        var i = string.startIndex
        while i != string.endIndex {
            let ch = string[i]

            switch (state, ch) {
            case (.start, "a"..."z"),
                 (.start, "A"..."Z"):
                state = .withinName(String(ch), startingAt: i)

            case (.start, "("):
                tokens.append(.openParen(at: i))
            case (.start, ")"):
                tokens.append(.closeParen(at: i))

            case (.start, "+"),
                 (.start, "-"),
                 (.start, "."),
                 (.start, "0"..."9"):
                state = .withinNumber(String(ch), startingAt: i)

            case (.start, ","),
                 (.start, _) where ch.isWhitespace:
                 break

            case (.withinNumber(let sofar, startingAt: let start), "0"..."9"),
                 (.withinNumber(let sofar, startingAt: let start), ".") where !sofar.contains("."):
                state = .withinNumber(sofar + String(ch), startingAt: start)

            case (.withinNumber, "."):
                throw SVGTransformSyntaxError(pathString: string, characterIndex: i)

            case (.withinNumber(let sofar, startingAt: let start), _):
                tokens.append(.number(try parseNumber(sofar, startingAt: start), at: start))
                state = .start
                continue // don't advance `i` (re-process the same character)

            case (.withinName(let sofar, startingAt: let start), "a"..."z"),
                 (.withinName(let sofar, startingAt: let start), "A"..."Z"):
                state = .withinName(sofar + String(ch), startingAt: start)

            case (.withinName(let sofar, startingAt: let start), _):
                tokens.append(.name(sofar, at: start))
                state = .start
                continue // don't advance `i` (re-process the same character)

            default:
                throw SVGTransformSyntaxError(pathString: string, characterIndex: i)
            }

            i = string.index(after: i)
        }

        switch state {
        case .withinNumber(let sofar, startingAt: let start):
            tokens.append(.number(try parseNumber(sofar, startingAt: start), at: start))
        case .withinName(let sofar, startingAt: let start):
            tokens.append(.name(sofar, at: start))
        case .start:
            break
        }

        return tokens
    }

    private func parseNumber(_ string: String, startingAt start: String.Index) throws -> CGFloat {
        guard let val = CGFloat(exactly: (string as NSString).doubleValue) else {
            throw SVGPathSyntaxError(pathString: string, characterIndex: start)
        }
        return val
    }

    private enum LexicalState {
        case start
        case withinName(String, startingAt: String.Index)
        case withinNumber(String, startingAt: String.Index)
    }
}
