//
//    CSSReader.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//


/// Implements the world's poorest excuse for a CSS parser.
///
/// SVG files sometimes have embedded CSS, either within `style` attributes or within
/// actual `<style>` elements. The SVG spec fully interoperates with CSS, but as
/// Yass is focused on icons and generally icons authored by drawing tools, I don't
/// care about most of it.
///
struct CSSReader {
    /// Reads a list of CSS rules of the form "selector { properties }".
    ///
    /// Only supports a couple of selector types; see `CSSSelector` for details.
    ///
    func readRules(_ string: String, strict: Bool) throws -> [CSSRule] {
        var rules: [CSSRule] = []
        var suffix = Substring(string)
        while let (s1, selector) = readSelector(suffix),
              let s2 = readChar("{")(s1),
              let (s3, attrs) = readPropertyList(s2),
              let s4 = readChar("}")(s3) {
            let pres = try SVGPresentationAttributeReader().readAttributes(attrs, fromCSS: true, strict: strict)
            rules.append(
                CSSRule(
                    selector: selector,
                    presentationAttributes: pres
                )
            )
            suffix = s4
        }
        return rules
    }

    /// Reads a CSS declaration list, producing a simple property name/value mapping.
    /// Performs no syntactic validation of property names or values beyond basic
    /// lexical splitting and bracket balancing.
    ///
    /// Recognizes a sequence of "property-name: property-value" declarations, separated
    /// by `;` characters, with intermixed comments and whitespace. Property names must
    /// be valid, but property values are pure opaque strings to this class, other than
    /// balancing of brackets/quotes/parentheses. If the same property name appears
    /// multiple times, later ones completely replace earlier ones.
    ///
    func readProperties(_ string: String) -> [String: String] {
        if let (_, result) = readPropertyList(Substring(string)) {
            return result
        }
        return [:]
    }


    private func readSelector(_ input: Substring) -> (Substring, CSSSelector)? {
        let input = skipSpaceAndComments(input)
        if input.starts(with: ".") {
            if let (suffix, name) = readIdentifier(input.dropFirst()) {
                return (suffix, .class(name))
            }

        } else if input.starts(with: "#") {
            if let (suffix, name) = readIdentifier(input.dropFirst()) {
                return (suffix, .id(name))
            }
        }

        return nil
    }

    private func readPropertyList(_ input: Substring) -> (Substring, [String: String])? {
        var suffix = input
        var result: [String: String] = [:]
        while !suffix.isEmpty {
            if let s = readChar(";")(suffix) {
                suffix = s
            } else if let (s1, pname) = readIdentifier(suffix),
                      let s2 = readChar(":")(s1),
                      let (s3, pvalue) = readPropertyValue(s2) {
                result[pname] = pvalue
                suffix = s3
            } else {
                break
            }
        }
        return (suffix, result)
    }

    /// Consumes any amount of whitespace and/or CSS comments.
    ///
    private func skipSpaceAndComments(_ input: Substring) -> Substring {
        var suffix = input
        while !suffix.isEmpty {
            if suffix.first!.isWhitespace {
                suffix = suffix.dropFirst()

            } else if suffix.starts(with: "/*") {
                suffix = suffix.dropFirst(2)
                if let end = suffix.indices.first(where: { i in suffix[i...].starts(with: "*/") }) {
                    suffix = suffix[end...].dropFirst(2)
                } else {
                    suffix = suffix[suffix.endIndex..<suffix.endIndex]
                }

            } else {
                break
            }
        }
        return suffix
    }

    /// Consumes a single CSS identifier: this includes properties, classes, and IDs.
    ///
    private func readIdentifier(_ input: Substring) -> (Substring, String)? {
        let suffix = skipSpaceAndComments(input)
        guard !suffix.isEmpty else {
            return nil
        }

        var i = suffix.startIndex
        switch suffix[i] {
        case "-", "a"..."z", "A"..."Z", "_":
            suffix.formIndex(after: &i)

        default:
            return nil
        }

        scan:
        while i < suffix.endIndex {
            switch suffix[i] {
            case "-", "a"..."z", "A"..."Z", "_", "0"..."9":
                suffix.formIndex(after: &i)

            default:
                break scan
            }
        }

        return (suffix[i...], String(suffix[..<i]))
    }

    /// Consumes a property value, advancing `i` to the next `;` or to the end of the string.
    /// CSS3 syntax allows empty property values. This doesn't check for syntactic validity
    /// within the property value. Note that the CSS spec allows empty property values, so so
    /// do I.
    ///
    private func readPropertyValue(_ input: Substring) -> (Substring, String)? {
        let suffix = skipSpaceAndComments(input)
        var nesting: [Character] = []

        var i = suffix.startIndex
        let startOfContent = i
        var endOfContent = i

        scan:
        while i < suffix.endIndex {
            let ch = suffix[i]
            switch ch {
            case ";" where nesting.isEmpty,
                 "}" where nesting.isEmpty:
                break scan

            case "\"", "'":
                i = suffix.index(after: i)
                i = suffix[i...].firstIndex(of: ch) ?? suffix.endIndex

            case "(", "[", "{":
                nesting.append(ch)

            case ")" where nesting.last == "(",
                 "]" where nesting.last == "[",
                 "}" where nesting.last == "{":
                nesting.removeLast()

            default:
                break
            }

            if !suffix[i].isWhitespace {
                endOfContent = suffix.index(after: i)
            }

            if i < suffix.endIndex {
                i = suffix.index(after: i)
            }
        }

        return (suffix[endOfContent...], String(suffix[startOfContent..<endOfContent]))
    }

    /// Consumes the given character (after skipping any whitespace/comments), if present.
    ///
    private func readChar(_ character: Character) -> (Substring) -> Substring? {
        return { input in
            let suffix = skipSpaceAndComments(input)
            if suffix.first == character {
                return suffix.dropFirst()
            }
            return nil
        }
    }
}

