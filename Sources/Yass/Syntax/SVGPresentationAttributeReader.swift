//
//    SVGPresentationAttributeReader.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation
import CoreGraphics


struct SVGPresentationAttributeSyntaxError: LocalizedError {
    var attributeName: String
    var attributeValue: String

    var errorDescription: String? { "unrecognized syntax for attribute \(attributeName): \"\(attributeValue)\""}
}


/// Implements basic reading of SVG presentation attributes.
///
struct SVGPresentationAttributeReader {
    /// Parses a set of name/value pairs containing SVG presentation attributes,
    /// from either SVG XML attributes or CSS properties.
    ///
    /// If `strict` is true, this throws on any invalid syntax; otherwise, it
    /// ignores it, thus causing the final rendering to fall back on inherited
    /// or default values.
    ///
    /// If `attr` contains a `style` attribute, and `fromCSS` is false, the
    /// inline properties are recursively parsed and applied as well.
    ///
    /// The resulting resolvable attribute set includes the appropriate CSS
    /// stylesheet lookups, as well, including any `id` or `class` attributes
    /// in the input XML. That lookup doesn't actually happen, of course, until
    /// you resolve it, which should occur only after loading the entire SVG.
    ///
    func readAttributes(_ xmlAttrs: [String: String], fromCSS: Bool = false, strict: Bool) throws -> SVGPresentationAttributeSet {
        let elementID = xmlAttrs["id"]
        let elementClasses = xmlAttrs["class"]?.svg_asCSSClassList ?? []

        var result = SVGPresentationAttributeSet(elementID: elementID, elementClasses: elementClasses)

        for attr in SVGPAs.all {
            guard
                let string = xmlAttrs[attr.name] ?? attr.altNames.compactMap({ xmlAttrs[$0] }).first
            else {
                continue
            }

            if let id = string.svg_asReferenceToID {
                result.set(refTo: id, for: attr)
                continue
            }

            do {
                if let val = try attr.read(string) {
                    result.set(value: val, for: attr)
                } else if strict {
                    throw SVGPresentationAttributeSyntaxError(attributeName: attr.name, attributeValue: string)
                }
            } catch {
                if strict {
                    throw error
                }
            }
        }

        if !fromCSS, let inlineCSS = xmlAttrs["style"] {
            let inlineAttributes = try readAttributes(CSSReader().readProperties(inlineCSS), fromCSS: true, strict: strict)
            result.merge(inlineAttributes)
        }

        return result
    }
}

extension SVGDrawingAttributes {
    init(_ attr: SVGPresentationAttributeSet, using lookup: SVGLookup) {
        self.init(
            display: attr.get(SVGPAs.Display.self, using: lookup),
            fill: attr.get(SVGPAs.Fill.self, using: lookup),
            fillRule: attr.get(SVGPAs.FillRule.self, using: lookup),
            fillOpacity: attr.get(SVGPAs.FillOpacity.self, using: lookup),
            stroke: attr.get(SVGPAs.Stroke.self, using: lookup),
            strokeOpacity: attr.get(SVGPAs.StrokeOpacity.self, using: lookup),
            strokeWidth: attr.get(SVGPAs.StrokeWidth.self, using: lookup),
            strokeLineCap: attr.get(SVGPAs.StrokeLineCap.self, using: lookup),
            strokeLineJoin: attr.get(SVGPAs.StrokeLineJoin.self, using: lookup),
            strokeMiterLimit: attr.get(SVGPAs.StrokeMiterLimit.self, using: lookup),
            strokeDashArray: attr.get(SVGPAs.StrokeDashArray.self, using: lookup),
            strokeDashOffset: attr.get(SVGPAs.StrokeDashOffset.self, using: lookup),
            transform: attr.get(SVGPAs.Transform.self, using: lookup),
            currentColor: attr.get(SVGPAs.CurrentColor.self, using: lookup)
        )
    }
}

