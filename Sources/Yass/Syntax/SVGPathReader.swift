//
//    SVGPathReader.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//


import Foundation
import CoreGraphics


public struct SVGPathSyntaxError: LocalizedError {
    var pathString: String
    var characterIndex: String.Index

    public var errorDescription: String? {
        var marked = pathString
        marked.insert("➤", at: characterIndex)
        return "syntax error in SVG path (marked with ➤): \"\(marked)\""
    }
}


/// An object which can parse SVG path syntax (the `d` attribute on a `path`
/// element), producing the sequence of `SVGPathInstruction`s which it describes.
///
public struct SVGPathReader {

    /// Reads a path string.
    ///
    /// Throws an SVGPathSyntaxError on any failures. If you want best-effort
    /// parsing (as called for in the SVG spec), you can trim the path string
    /// at the error's character index and reparse. Note that I treat any error
    /// as a "syntax error".
    ///
    /// This method converts all relative coordinates into absolute coordinates.
    ///
    /// Notes:
    /// - I use `Character.isWhitespace` as the whitespace definition; this is much
    ///   broader than the SVG specification.
    /// - I treat commas as whitespace everywhere; this is much more lenient than
    ///   the SVG specification, which only allows optional commas in some places.
    ///
    public func read(_ string: String) throws -> [SVGPathInstruction] {
        var ctx = Context(pathString: string, startOfCommand: string.startIndex)
        var lexer = SVGPathLexer(string)

        while let (commandName, startOfCommand) = lexer.scanCommand() {
            ctx.startOfCommand = startOfCommand

            switch commandName {

            // ==============================
            // MARK: General path commands

            case "M", "m":
                // Move to point; subsequent args are implicit line-to-points
                let point = try getPoint(from: &lexer, relative: commandName == "m", in: ctx)
                ctx.initialPoint = point
                ctx.add(.moveTo(point))
                while lexer.argsAvailable {
                    ctx.add(.lineTo(try getPoint(from: &lexer, relative: commandName == "m", in: ctx)))
                }

            case _ where ctx.path.isEmpty:
                // Any command other than M/m at the start of a path is an error
                throw SVGPathSyntaxError(pathString: string, characterIndex: ctx.startOfCommand)

            case "Z", "z":
                // Close path (Z/z are equivalent)
                ctx.add(.closePath)

            // ==============================
            // MARK: Line-drawing commands

            case "L", "l":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let point = try getPoint(from: &lexer, relative: commandName == "l", in: ctx)
                    ctx.add(.lineTo(point))
                }

            case "H", "h":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let base = commandName == "h" ? ctx.currentPoint.x : 0
                    let x = try getFloat(from: &lexer, relativeTo: base, in: ctx)
                    let point = CGPoint(x: x, y: ctx.currentPoint.y)
                    ctx.add(.lineTo(point))
                }

            case "V", "v":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let base = commandName == "v" ? ctx.currentPoint.y : 0
                    let y = try getFloat(from: &lexer, relativeTo: base, in: ctx)
                    let point = CGPoint(x: ctx.currentPoint.x, y: y)
                    ctx.add(.lineTo(point))
                }

            // ==============================
            // MARK: Bézier commands

            case "C", "c":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let c1 = try getPoint(from: &lexer, relative: commandName == "c", in: ctx)
                    let c2 = try getPoint(from: &lexer, relative: commandName == "c", in: ctx)
                    let end = try getPoint(from: &lexer, relative: commandName == "c", in: ctx)

                    ctx.add(.cubicCurveTo(end, control1: c1, control2: c2))
                }

            case "S", "s":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let c1: CGPoint
                    if case .cubicCurveTo(_, control1: _, control2: let prevC2) = ctx.lastCommand {
                        c1 = reflectionOf(prevC2, around: ctx.currentPoint)
                    } else {
                        c1 = ctx.currentPoint
                    }

                    let c2 = try getPoint(from: &lexer, relative: commandName == "s", in: ctx)
                    let end = try getPoint(from: &lexer, relative: commandName == "s", in: ctx)

                    ctx.add(.cubicCurveTo(end, control1: c1, control2: c2))
                }

            case "Q", "q":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let c = try getPoint(from: &lexer, relative: commandName == "q", in: ctx)
                    let end = try getPoint(from: &lexer, relative: commandName == "q", in: ctx)

                    ctx.add(.quadCurveTo(end, control: c))
                }

            case "T", "t":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let c: CGPoint
                    if case .quadCurveTo(_, control: let prevCP) = ctx.lastCommand {
                        c = reflectionOf(prevCP, around: ctx.currentPoint)
                    } else {
                        c = ctx.currentPoint
                    }

                    let end = try getPoint(from: &lexer, relative: commandName == "t", in: ctx)
                    ctx.add(.quadCurveTo(end, control: c))
                }

            // ==============================
            // MARK: Arc commands

            case "A", "a":
                try atLeastOnce(from: &lexer, in: ctx) { lexer in
                    let rx = try getFloat(from: &lexer, in: ctx)
                    let ry = try getFloat(from: &lexer, in: ctx)
                    let rot = try getFloat(from: &lexer, in: ctx)
                    let largeArc = try getFlag(from: &lexer, in: ctx)
                    let sweep = try getFlag(from: &lexer, in: ctx)
                    let end = try getPoint(from: &lexer, relative: commandName == "a", in: ctx)

                    ctx.add(.arcTo(end, radiusX: rx, radiusY: ry, rotation: rot, largeArc: largeArc, sweep: sweep))
                }

            // ==============================
            // MARK: Unsupported/illegal commands

            default:
                throw SVGPathSyntaxError(pathString: string, characterIndex: ctx.startOfCommand)
            }
        }

        return ctx.path
    }

    private struct Context {
        var pathString: String
        var startOfCommand: String.Index
        var initialPoint: CGPoint = .zero

        private(set) var currentPoint: CGPoint = .zero
        private(set) var path: [SVGPathInstruction] = []
        var lastCommand: SVGPathInstruction { path.last ?? .closePath }

        mutating func add(_ command: SVGPathInstruction) {
            path.append(command)

            switch command {
            case .closePath:
                currentPoint = initialPoint

            case .arcTo(let p, radiusX: _, radiusY: _, rotation: _, largeArc: _, sweep: _),
                 .cubicCurveTo(let p, control1: _, control2: _),
                 .lineTo(let p),
                 .moveTo(let p),
                 .quadCurveTo(let p, control: _):
                currentPoint = p
            }
        }
    }

    // MARK: - Support functions

    private func getPoint(from lexer: inout SVGPathLexer,
                          relative: Bool = false,
                          in ctx: Context) throws -> CGPoint {
        guard
            let (x, _) = try lexer.scanNumber(),
            let (y, _) = try lexer.scanNumber()
        else {
            throw SVGPathSyntaxError(pathString: ctx.pathString, characterIndex: ctx.startOfCommand)
        }
        if relative {
            return CGPoint(x: ctx.currentPoint.x + x, y: ctx.currentPoint.y + y)
        } else {
            return CGPoint(x: x, y: y)
        }
    }

    private func getFloat(from lexer: inout SVGPathLexer,
                          relativeTo: CGFloat = 0,
                          in ctx: Context) throws -> CGFloat {
        guard let (x, _) = try lexer.scanNumber() else {
            throw SVGPathSyntaxError(pathString: ctx.pathString, characterIndex: ctx.startOfCommand)
        }
        return x + relativeTo
    }

    private func getFlag(from lexer: inout SVGPathLexer,
                         in ctx: Context) throws -> Bool {
        guard let (f, _) = lexer.scanFlag() else {
            throw SVGPathSyntaxError(pathString: ctx.pathString, characterIndex: ctx.startOfCommand)
        }
        return f
    }

    private func atLeastOnce(from lexer: inout SVGPathLexer, in ctx: Context, do fn: (inout SVGPathLexer) throws -> ()) throws {
        if !lexer.argsAvailable {
            throw SVGPathSyntaxError(pathString: ctx.pathString, characterIndex: ctx.startOfCommand)
        }
        while lexer.argsAvailable {
            try fn(&lexer)
        }
    }


    private func reflectionOf(_ old: CGPoint, around cur: CGPoint) -> CGPoint {
        CGPoint(x: 2 * cur.x - old.x, y: 2 * cur.y - old.y)
    }
}

// MARK: - Lexing

internal struct SVGPathLexer {
    init(_ pathString: String) {
        self.input = pathString
        if pathString == "none" {
            self.index = pathString.endIndex
            return
        }

        self.index = pathString.startIndex
        advanceToNextNonWhitespace()
    }

    var atEnd: Bool {
        return index == input.endIndex
    }

    /// True if we're not at the end, and the next thing to be read isn't
    /// a new command.
    ///
    var argsAvailable: Bool {
        return !atEnd && peekCommand() == nil
    }

    mutating func scanCommand() -> (Character, String.Index)? {
        if let result = peekCommand() {
            input.formIndex(after: &index)
            advanceToNextNonWhitespace()
            return result
        }
        return nil
    }

    private func peekCommand() -> (Character, String.Index)? {
        guard index < input.endIndex else {
            return nil
        }

        switch input[index] {
        case "a"..."z", "A"..."Z":
            return (input[index], index)
        default:
            return nil
        }
    }

    mutating func scanNumber() throws -> (CGFloat, String.Index)? {
        guard index < input.endIndex else {
            return nil
        }

        enum State {
            case sign
            case whole
            case frac
            case expsign
            case exp
        }

        let startIndex = index
        var state = State.sign

        scanloop:
        while index < input.endIndex {
            let ch = input[index]
            switch (state, ch) {
            case (.sign, "+"), (.sign, "-"):
                state = .whole
            case (.sign, _) where ch.isASCII && ch.isNumber:
                state = .whole
            case (.sign, "."):
                state = .frac
            case (.whole, "."):
                state = .frac
            case (.whole, "e"), (.whole, "E"), (.frac, "e"), (.frac, "E"):
                state = .expsign
            case (.expsign, "+"),
                 (.expsign, "-"),
                 (.expsign, _) where ch.isASCII && ch.isNumber:
                state = .exp
            case (.frac, _) where ch.isASCII && ch.isNumber,
                 (.whole, _) where ch.isASCII && ch.isNumber,
                 (.exp, _) where ch.isASCII && ch.isNumber:
                break
            case (.sign, _), (.whole, _), (.frac, _), (.expsign, _), (.exp, _):
                break scanloop
            }
            input.formIndex(after: &index)
        }

        guard let dbl = Double(input[startIndex..<index]) else {
            throw SVGPathSyntaxError(pathString: String(input[startIndex..<index]), characterIndex: startIndex)
        }

        advanceToNextNonWhitespace()
        return (CGFloat(dbl), startIndex)
    }

    mutating func scanFlag() -> (Bool, String.Index)? {
        guard index < input.endIndex else {
            return nil
        }

        switch input[index] {
        case "1":
            let result = (true, index)
            input.formIndex(after: &index)
            advanceToNextNonWhitespace()
            return result

        case "0":
            let result = (false, index)
            input.formIndex(after: &index)
            advanceToNextNonWhitespace()
            return result

        default:
            return nil
        }
    }

    private mutating func advanceToNextNonWhitespace() {
        while index < input.endIndex, input[index] == "," || input[index].isWhitespace {
            index = input.index(after: index)
        }
    }

    private let input: String
    private var index: String.Index
}
