//
//    SVGPaintReader.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation
import CoreGraphics


struct SVGPaintReader {
    func read(_ str: String) -> SVGPaint? {
        let str = str.lowercased()

        if str == "none" {
            return SVGPaint.none
        }

        if str == "currentcolor" {
            return .currentColor
        }

        if let color = Self.colorNames[str] {
            return color
        }

        if str.starts(with: "#") {
            let hexString = String(str.dropFirst())
            guard let u64 = UInt64(hexString, radix: 16) else {
                return nil
            }
            let r, g, b, a: CGFloat
            switch hexString.count {
            case 8:
                r = CGFloat((u64 >> 24) & 0xff) / 0xff
                g = CGFloat((u64 >> 16) & 0xff) / 0xff
                b = CGFloat((u64 >>  8) & 0xff) / 0xff
                a = CGFloat( u64        & 0xff) / 0xff
            case 6:
                r = CGFloat((u64 >> 16) & 0xff) / 0xff
                g = CGFloat((u64 >>  8) & 0xff) / 0xff
                b = CGFloat( u64        & 0xff) / 0xff
                a = 1.0
            case 3:
                r = CGFloat((u64 >>  8) & 0x0f) / 0x0f
                g = CGFloat((u64 >>  4) & 0x0f) / 0x0f
                b = CGFloat( u64        & 0x0f) / 0x0f
                a = 1.0
            default:
                return nil
            }
            return .rgba(r, g, b, a)
        }

        return nil
    }

    // source: https://drafts.csswg.org/css-color/#named-colors
    //
    private static let colorNames: [String: SVGPaint] = [
        "aliceblue"       : SVGPaint.rgba(240.0 / 255.0, 248.0 / 255.0, 255.0 / 255.0, 1),
        "antiquewhite"    : SVGPaint.rgba(250.0 / 255.0, 235.0 / 255.0, 215.0 / 255.0, 1),
        "aqua"            : SVGPaint.rgba(0.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 1),
        "aquamarine"      : SVGPaint.rgba(127.0 / 255.0, 255.0 / 255.0, 212.0 / 255.0, 1),
        "azure"           : SVGPaint.rgba(240.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 1),
        "beige"           : SVGPaint.rgba(245.0 / 255.0, 245.0 / 255.0, 220.0 / 255.0, 1),
        "bisque"          : SVGPaint.rgba(255.0 / 255.0, 228.0 / 255.0, 196.0 / 255.0, 1),
        "black"           : SVGPaint.rgba(0.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 1),
        "blanchedalmond"  : SVGPaint.rgba(255.0 / 255.0, 235.0 / 255.0, 205.0 / 255.0, 1),
        "blue"            : SVGPaint.rgba(0.0 / 255.0, 0.0 / 255.0, 255.0 / 255.0, 1),
        "blueviolet"      : SVGPaint.rgba(138.0 / 255.0, 43.0 / 255.0, 226.0 / 255.0, 1),
        "brown"           : SVGPaint.rgba(165.0 / 255.0, 42.0 / 255.0, 42.0 / 255.0, 1),
        "burlywood"       : SVGPaint.rgba(222.0 / 255.0, 184.0 / 255.0, 135.0 / 255.0, 1),
        "cadetblue"       : SVGPaint.rgba(95.0 / 255.0, 158.0 / 255.0, 160.0 / 255.0, 1),
        "chartreuse"      : SVGPaint.rgba(127.0 / 255.0, 255.0 / 255.0, 0.0 / 255.0, 1),
        "chocolate"       : SVGPaint.rgba(210.0 / 255.0, 105.0 / 255.0, 30.0 / 255.0, 1),
        "coral"           : SVGPaint.rgba(255.0 / 255.0, 127.0 / 255.0, 80.0 / 255.0, 1),
        "cornflowerblue"  : SVGPaint.rgba(100.0 / 255.0, 149.0 / 255.0, 237.0 / 255.0, 1),
        "cornsilk"        : SVGPaint.rgba(255.0 / 255.0, 248.0 / 255.0, 220.0 / 255.0, 1),
        "crimson"         : SVGPaint.rgba(220.0 / 255.0, 20.0 / 255.0, 60.0 / 255.0, 1),
        "cyan"            : SVGPaint.rgba(0.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 1),
        "darkblue"        : SVGPaint.rgba(0.0 / 255.0, 0.0 / 255.0, 139.0 / 255.0, 1),
        "darkcyan"        : SVGPaint.rgba(0.0 / 255.0, 139.0 / 255.0, 139.0 / 255.0, 1),
        "darkgoldenrod"   : SVGPaint.rgba(184.0 / 255.0, 134.0 / 255.0, 11.0 / 255.0, 1),
        "darkgray"        : SVGPaint.rgba(169.0 / 255.0, 169.0 / 255.0, 169.0 / 255.0, 1),
        "darkgreen"       : SVGPaint.rgba(0.0 / 255.0, 100.0 / 255.0, 0.0 / 255.0, 1),
        "darkgrey"        : SVGPaint.rgba(169.0 / 255.0, 169.0 / 255.0, 169.0 / 255.0, 1),
        "darkkhaki"       : SVGPaint.rgba(189.0 / 255.0, 183.0 / 255.0, 107.0 / 255.0, 1),
        "darkmagenta"     : SVGPaint.rgba(139.0 / 255.0, 0.0 / 255.0, 139.0 / 255.0, 1),
        "darkolivegreen"  : SVGPaint.rgba(85.0 / 255.0, 107.0 / 255.0, 47.0 / 255.0, 1),
        "darkorange"      : SVGPaint.rgba(255.0 / 255.0, 140.0 / 255.0, 0.0 / 255.0, 1),
        "darkorchid"      : SVGPaint.rgba(153.0 / 255.0, 50.0 / 255.0, 204.0 / 255.0, 1),
        "darkred"         : SVGPaint.rgba(139.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 1),
        "darksalmon"      : SVGPaint.rgba(233.0 / 255.0, 150.0 / 255.0, 122.0 / 255.0, 1),
        "darkseagreen"    : SVGPaint.rgba(143.0 / 255.0, 188.0 / 255.0, 143.0 / 255.0, 1),
        "darkslateblue"   : SVGPaint.rgba(72.0 / 255.0, 61.0 / 255.0, 139.0 / 255.0, 1),
        "darkslategray"   : SVGPaint.rgba(47.0 / 255.0, 79.0 / 255.0, 79.0 / 255.0, 1),
        "darkslategrey"   : SVGPaint.rgba(47.0 / 255.0, 79.0 / 255.0, 79.0 / 255.0, 1),
        "darkturquoise"   : SVGPaint.rgba(0.0 / 255.0, 206.0 / 255.0, 209.0 / 255.0, 1),
        "darkviolet"      : SVGPaint.rgba(148.0 / 255.0, 0.0 / 255.0, 211.0 / 255.0, 1),
        "deeppink"        : SVGPaint.rgba(255.0 / 255.0, 20.0 / 255.0, 147.0 / 255.0, 1),
        "deepskyblue"     : SVGPaint.rgba(0.0 / 255.0, 191.0 / 255.0, 255.0 / 255.0, 1),
        "dimgray"         : SVGPaint.rgba(105.0 / 255.0, 105.0 / 255.0, 105.0 / 255.0, 1),
        "dimgrey"         : SVGPaint.rgba(105.0 / 255.0, 105.0 / 255.0, 105.0 / 255.0, 1),
        "dodgerblue"      : SVGPaint.rgba(30.0 / 255.0, 144.0 / 255.0, 255.0 / 255.0, 1),
        "firebrick"       : SVGPaint.rgba(178.0 / 255.0, 34.0 / 255.0, 34.0 / 255.0, 1),
        "floralwhite"     : SVGPaint.rgba(255.0 / 255.0, 250.0 / 255.0, 240.0 / 255.0, 1),
        "forestgreen"     : SVGPaint.rgba(34.0 / 255.0, 139.0 / 255.0, 34.0 / 255.0, 1),
        "fuchsia"         : SVGPaint.rgba(255.0 / 255.0, 0.0 / 255.0, 255.0 / 255.0, 1),
        "gainsboro"       : SVGPaint.rgba(220.0 / 255.0, 220.0 / 255.0, 220.0 / 255.0, 1),
        "ghostwhite"      : SVGPaint.rgba(248.0 / 255.0, 248.0 / 255.0, 255.0 / 255.0, 1),
        "gold"            : SVGPaint.rgba(255.0 / 255.0, 215.0 / 255.0, 0.0 / 255.0, 1),
        "goldenrod"       : SVGPaint.rgba(218.0 / 255.0, 165.0 / 255.0, 32.0 / 255.0, 1),
        "gray"            : SVGPaint.rgba(128.0 / 255.0, 128.0 / 255.0, 128.0 / 255.0, 1),
        "green"           : SVGPaint.rgba(0.0 / 255.0, 128.0 / 255.0, 0.0 / 255.0, 1),
        "greenyellow"     : SVGPaint.rgba(173.0 / 255.0, 255.0 / 255.0, 47.0 / 255.0, 1),
        "grey"            : SVGPaint.rgba(128.0 / 255.0, 128.0 / 255.0, 128.0 / 255.0, 1),
        "honeydew"        : SVGPaint.rgba(240.0 / 255.0, 255.0 / 255.0, 240.0 / 255.0, 1),
        "hotpink"         : SVGPaint.rgba(255.0 / 255.0, 105.0 / 255.0, 180.0 / 255.0, 1),
        "indianred"       : SVGPaint.rgba(205.0 / 255.0, 92.0 / 255.0, 92.0 / 255.0, 1),
        "indigo"          : SVGPaint.rgba(75.0 / 255.0, 0.0 / 255.0, 130.0 / 255.0, 1),
        "ivory"           : SVGPaint.rgba(255.0 / 255.0, 255.0 / 255.0, 240.0 / 255.0, 1),
        "khaki"           : SVGPaint.rgba(240.0 / 255.0, 230.0 / 255.0, 140.0 / 255.0, 1),
        "lavender"        : SVGPaint.rgba(230.0 / 255.0, 230.0 / 255.0, 250.0 / 255.0, 1),
        "lavenderblush"   : SVGPaint.rgba(255.0 / 255.0, 240.0 / 255.0, 245.0 / 255.0, 1),
        "lawngreen"       : SVGPaint.rgba(124.0 / 255.0, 252.0 / 255.0, 0.0 / 255.0, 1),
        "lemonchiffon"    : SVGPaint.rgba(255.0 / 255.0, 250.0 / 255.0, 205.0 / 255.0, 1),
        "lightblue"       : SVGPaint.rgba(173.0 / 255.0, 216.0 / 255.0, 230.0 / 255.0, 1),
        "lightcoral"      : SVGPaint.rgba(240.0 / 255.0, 128.0 / 255.0, 128.0 / 255.0, 1),
        "lightcyan"       : SVGPaint.rgba(224.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 1),
        "lightgoldenrodyellow": SVGPaint.rgba(250.0 / 255.0, 250.0 / 255.0, 210.0 / 255.0, 1),
        "lightgray"       : SVGPaint.rgba(211.0 / 255.0, 211.0 / 255.0, 211.0 / 255.0, 1),
        "lightgreen"      : SVGPaint.rgba(144.0 / 255.0, 238.0 / 255.0, 144.0 / 255.0, 1),
        "lightgrey"       : SVGPaint.rgba(211.0 / 255.0, 211.0 / 255.0, 211.0 / 255.0, 1),
        "lightpink"       : SVGPaint.rgba(255.0 / 255.0, 182.0 / 255.0, 193.0 / 255.0, 1),
        "lightsalmon"     : SVGPaint.rgba(255.0 / 255.0, 160.0 / 255.0, 122.0 / 255.0, 1),
        "lightseagreen"   : SVGPaint.rgba(32.0 / 255.0, 178.0 / 255.0, 170.0 / 255.0, 1),
        "lightskyblue"    : SVGPaint.rgba(135.0 / 255.0, 206.0 / 255.0, 250.0 / 255.0, 1),
        "lightslategray"  : SVGPaint.rgba(119.0 / 255.0, 136.0 / 255.0, 153.0 / 255.0, 1),
        "lightslategrey"  : SVGPaint.rgba(119.0 / 255.0, 136.0 / 255.0, 153.0 / 255.0, 1),
        "lightsteelblue"  : SVGPaint.rgba(176.0 / 255.0, 196.0 / 255.0, 222.0 / 255.0, 1),
        "lightyellow"     : SVGPaint.rgba(255.0 / 255.0, 255.0 / 255.0, 224.0 / 255.0, 1),
        "lime"            : SVGPaint.rgba(0.0 / 255.0, 255.0 / 255.0, 0.0 / 255.0, 1),
        "limegreen"       : SVGPaint.rgba(50.0 / 255.0, 205.0 / 255.0, 50.0 / 255.0, 1),
        "linen"           : SVGPaint.rgba(250.0 / 255.0, 240.0 / 255.0, 230.0 / 255.0, 1),
        "magenta"         : SVGPaint.rgba(255.0 / 255.0, 0.0 / 255.0, 255.0 / 255.0, 1),
        "maroon"          : SVGPaint.rgba(128.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 1),
        "mediumaquamarine": SVGPaint.rgba(102.0 / 255.0, 205.0 / 255.0, 170.0 / 255.0, 1),
        "mediumblue"      : SVGPaint.rgba(0.0 / 255.0, 0.0 / 255.0, 205.0 / 255.0, 1),
        "mediumorchid"    : SVGPaint.rgba(186.0 / 255.0, 85.0 / 255.0, 211.0 / 255.0, 1),
        "mediumpurple"    : SVGPaint.rgba(147.0 / 255.0, 112.0 / 255.0, 219.0 / 255.0, 1),
        "mediumseagreen"  : SVGPaint.rgba(60.0 / 255.0, 179.0 / 255.0, 113.0 / 255.0, 1),
        "mediumslateblue" : SVGPaint.rgba(123.0 / 255.0, 104.0 / 255.0, 238.0 / 255.0, 1),
        "mediumspringgreen": SVGPaint.rgba(0.0 / 255.0, 250.0 / 255.0, 154.0 / 255.0, 1),
        "mediumturquoise" : SVGPaint.rgba(72.0 / 255.0, 209.0 / 255.0, 204.0 / 255.0, 1),
        "mediumvioletred" : SVGPaint.rgba(199.0 / 255.0, 21.0 / 255.0, 133.0 / 255.0, 1),
        "midnightblue"    : SVGPaint.rgba(25.0 / 255.0, 25.0 / 255.0, 112.0 / 255.0, 1),
        "mintcream"       : SVGPaint.rgba(245.0 / 255.0, 255.0 / 255.0, 250.0 / 255.0, 1),
        "mistyrose"       : SVGPaint.rgba(255.0 / 255.0, 228.0 / 255.0, 225.0 / 255.0, 1),
        "moccasin"        : SVGPaint.rgba(255.0 / 255.0, 228.0 / 255.0, 181.0 / 255.0, 1),
        "navajowhite"     : SVGPaint.rgba(255.0 / 255.0, 222.0 / 255.0, 173.0 / 255.0, 1),
        "navy"            : SVGPaint.rgba(0.0 / 255.0, 0.0 / 255.0, 128.0 / 255.0, 1),
        "oldlace"         : SVGPaint.rgba(253.0 / 255.0, 245.0 / 255.0, 230.0 / 255.0, 1),
        "olive"           : SVGPaint.rgba(128.0 / 255.0, 128.0 / 255.0, 0.0 / 255.0, 1),
        "olivedrab"       : SVGPaint.rgba(107.0 / 255.0, 142.0 / 255.0, 35.0 / 255.0, 1),
        "orange"          : SVGPaint.rgba(255.0 / 255.0, 165.0 / 255.0, 0.0 / 255.0, 1),
        "orangered"       : SVGPaint.rgba(255.0 / 255.0, 69.0 / 255.0, 0.0 / 255.0, 1),
        "orchid"          : SVGPaint.rgba(218.0 / 255.0, 112.0 / 255.0, 214.0 / 255.0, 1),
        "palegoldenrod"   : SVGPaint.rgba(238.0 / 255.0, 232.0 / 255.0, 170.0 / 255.0, 1),
        "palegreen"       : SVGPaint.rgba(152.0 / 255.0, 251.0 / 255.0, 152.0 / 255.0, 1),
        "paleturquoise"   : SVGPaint.rgba(175.0 / 255.0, 238.0 / 255.0, 238.0 / 255.0, 1),
        "palevioletred"   : SVGPaint.rgba(219.0 / 255.0, 112.0 / 255.0, 147.0 / 255.0, 1),
        "papayawhip"      : SVGPaint.rgba(255.0 / 255.0, 239.0 / 255.0, 213.0 / 255.0, 1),
        "peachpuff"       : SVGPaint.rgba(255.0 / 255.0, 218.0 / 255.0, 185.0 / 255.0, 1),
        "peru"            : SVGPaint.rgba(205.0 / 255.0, 133.0 / 255.0, 63.0 / 255.0, 1),
        "pink"            : SVGPaint.rgba(255.0 / 255.0, 192.0 / 255.0, 203.0 / 255.0, 1),
        "plum"            : SVGPaint.rgba(221.0 / 255.0, 160.0 / 255.0, 221.0 / 255.0, 1),
        "powderblue"      : SVGPaint.rgba(176.0 / 255.0, 224.0 / 255.0, 230.0 / 255.0, 1),
        "purple"          : SVGPaint.rgba(128.0 / 255.0, 0.0 / 255.0, 128.0 / 255.0, 1),
        "rebeccapurple"   : SVGPaint.rgba(102.0 / 255.0, 51.0 / 255.0, 153.0 / 255.0, 1),
        "red"             : SVGPaint.rgba(255.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0, 1),
        "rosybrown"       : SVGPaint.rgba(188.0 / 255.0, 143.0 / 255.0, 143.0 / 255.0, 1),
        "royalblue"       : SVGPaint.rgba(65.0 / 255.0, 105.0 / 255.0, 225.0 / 255.0, 1),
        "saddlebrown"     : SVGPaint.rgba(139.0 / 255.0, 69.0 / 255.0, 19.0 / 255.0, 1),
        "salmon"          : SVGPaint.rgba(250.0 / 255.0, 128.0 / 255.0, 114.0 / 255.0, 1),
        "sandybrown"      : SVGPaint.rgba(244.0 / 255.0, 164.0 / 255.0, 96.0 / 255.0, 1),
        "seagreen"        : SVGPaint.rgba(46.0 / 255.0, 139.0 / 255.0, 87.0 / 255.0, 1),
        "seashell"        : SVGPaint.rgba(255.0 / 255.0, 245.0 / 255.0, 238.0 / 255.0, 1),
        "sienna"          : SVGPaint.rgba(160.0 / 255.0, 82.0 / 255.0, 45.0 / 255.0, 1),
        "silver"          : SVGPaint.rgba(192.0 / 255.0, 192.0 / 255.0, 192.0 / 255.0, 1),
        "skyblue"         : SVGPaint.rgba(135.0 / 255.0, 206.0 / 255.0, 235.0 / 255.0, 1),
        "slateblue"       : SVGPaint.rgba(106.0 / 255.0, 90.0 / 255.0, 205.0 / 255.0, 1),
        "slategray"       : SVGPaint.rgba(112.0 / 255.0, 128.0 / 255.0, 144.0 / 255.0, 1),
        "slategrey"       : SVGPaint.rgba(112.0 / 255.0, 128.0 / 255.0, 144.0 / 255.0, 1),
        "snow"            : SVGPaint.rgba(255.0 / 255.0, 250.0 / 255.0, 250.0 / 255.0, 1),
        "springgreen"     : SVGPaint.rgba(0.0 / 255.0, 255.0 / 255.0, 127.0 / 255.0, 1),
        "steelblue"       : SVGPaint.rgba(70.0 / 255.0, 130.0 / 255.0, 180.0 / 255.0, 1),
        "tan"             : SVGPaint.rgba(210.0 / 255.0, 180.0 / 255.0, 140.0 / 255.0, 1),
        "teal"            : SVGPaint.rgba(0.0 / 255.0, 128.0 / 255.0, 128.0 / 255.0, 1),
        "thistle"         : SVGPaint.rgba(216.0 / 255.0, 191.0 / 255.0, 216.0 / 255.0, 1),
        "tomato"          : SVGPaint.rgba(255.0 / 255.0, 99.0 / 255.0, 71.0 / 255.0, 1),
        "turquoise"       : SVGPaint.rgba(64.0 / 255.0, 224.0 / 255.0, 208.0 / 255.0, 1),
        "violet"          : SVGPaint.rgba(238.0 / 255.0, 130.0 / 255.0, 238.0 / 255.0, 1),
        "wheat"           : SVGPaint.rgba(245.0 / 255.0, 222.0 / 255.0, 179.0 / 255.0, 1),
        "white"           : SVGPaint.rgba(255.0 / 255.0, 255.0 / 255.0, 255.0 / 255.0, 1),
        "whitesmoke"      : SVGPaint.rgba(245.0 / 255.0, 245.0 / 255.0, 245.0 / 255.0, 1),
        "yellow"          : SVGPaint.rgba(255.0 / 255.0, 255.0 / 255.0, 0.0 / 255.0, 1),
        "yellowgreen"     : SVGPaint.rgba(154.0 / 255.0, 205.0 / 255.0, 50.0 / 255.0, 1),
    ]
}
