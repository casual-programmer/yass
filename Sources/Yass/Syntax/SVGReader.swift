//
//    SVGReader.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//


import Foundation
import CoreGraphics


public struct SVGSyntaxError: LocalizedError {
    init(parser: XMLParser, message: String) {
        self.line = parser.lineNumber
        self.column = parser.columnNumber
        self.message = message
    }

    let line: Int
    let column: Int
    let message: String?

    public var errorDescription: String? {
        if let message = message {
            return "SVG parsing error at \(line):\(column): \(message)"
        } else {
            return "SVG parsing error at \(line):\(column)"
        }
    }
}


public struct SVGReader {
    public init() { }

    public var allowAndIgnoreForeignNamespaces = true
    public var allowAndIgnoreUnsupportedSVG = true

    public func read(data: Data, failFast: Bool = false) throws -> SVGElement {
        let parser = XMLParser(data: data)
        let delegate = SVGXMLParserDelegate(reader: self, failFast: failFast)
        parser.delegate = delegate
        parser.shouldProcessNamespaces = true
        guard parser.parse(), let rootElement = delegate.finishedRootElement else {
            throw delegate.error ?? SVGSyntaxError(parser: parser, message: parser.parserError?.localizedDescription ?? "invalid SVG file")
        }
        return rootElement
    }
}



class SVGXMLParserDelegate: NSObject, XMLParserDelegate {
    static let xmlnsSVG = "http://www.w3.org/2000/svg"

    init(reader: SVGReader, failFast: Bool) {
        self.reader = reader
        self.failFast = failFast
    }

    private(set) var finishedRootElement: SVGElement? = nil
    private(set) var error: Error? = nil

    private let reader: SVGReader
    private var definitions = SVGLookup()
    private let failFast: Bool

    private var stack: [StackItem] = []
    private enum StackItem {
        case finishedElement(SVGResolvable<SVGElement>)

        case svg(id: String?, fattr: SVGFragmentAttributes, pattr: SVGPresentationAttributeSet, kids: [SVGResolvable<SVGElement>])
        case group(id: String?, pattr: SVGPresentationAttributeSet, kids: [SVGResolvable<SVGElement>])

        case linearGradientDefinition(id: String, x1: SVGDistanceOrPercentage, y1: SVGDistanceOrPercentage, x2: SVGDistanceOrPercentage, y2: SVGDistanceOrPercentage, pattr: SVGPresentationAttributeSet, stops: [SVGResolvable<SVGGradientStop>])

        case skip
        case style(String)
        case defsWrapper
    }

    private func assembledPresAttr(attr: [String: String]) throws -> SVGPresentationAttributeSet {
        var result = try SVGPresentationAttributeReader().readAttributes(attr, strict: failFast)

        switch stack.last {
        case nil, .skip, .defsWrapper, .style, .linearGradientDefinition, .finishedElement:
            break
        case .svg(_, _, let parentAttrSet, _),
             .group(_, let parentAttrSet, _):
            result.inherit(from: parentAttrSet)
        }

        return result
    }

    private func resovableWithPresAttr<T>(_ attr: [String: String], _ maker: @escaping (SVGPresentationAttributeSet, SVGLookup) -> T) throws -> SVGResolvable<T> {
        let pattr = try assembledPresAttr(attr: attr)
        return SVGResolvable { lookup in maker(pattr, lookup) }
    }

    private func pushFinishedElement(_ attr: [String: String], _ maker: @escaping (SVGDrawingAttributes) -> SVGElement) throws {
        let pattr = try assembledPresAttr(attr: attr)
        let resolvable = SVGResolvable { lookup in maker(SVGDrawingAttributes(pattr, using: lookup)) }
        stack.append(.finishedElement(resolvable))
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attr: [String : String] = [:]) {
        if case .skip = stack.last {
            stack.append(.skip) // skipping -> skip whole subtree
            return
        }

        func require<T>(_ attrName: String, _ getter: (String) -> T?) throws -> T {
            guard
                let str = attr[attrName],
                let value = getter(str)
            else {
                throw SVGSyntaxError(parser: parser, message: "Missing or invalid attribute \(attrName)")
            }
            return value
        }

        do {
            guard namespaceURI == SVGXMLParserDelegate.xmlnsSVG else {
                if reader.allowAndIgnoreForeignNamespaces {
                    stack.append(.skip)
                    return
                }
                throw SVGSyntaxError(parser: parser, message: "expected SVG namespace, found \(namespaceURI ?? "none")")
            }

            let id = attr["id"]

            switch elementName {
            case "style":
                stack.append(.style(""))

            case "desc", "title":
                stack.append(.skip)

            case "svg":
                var fragmentAttrs = SVGFragmentAttributes()

                if let box = attr["viewBox"]?.svg_asFloatList {
                    if box.count == 4 {
                        fragmentAttrs.viewBox = CGRect(x: box[0], y: box[1], width: box[2], height: box[3])
                    } else if failFast {
                        throw SVGSyntaxError(parser: parser, message: "viewBox needs four coordinates")
                    }
                }

                // If no viewBox was given, but a viewport size is given, assume
                // a view box of that size as well.
                if fragmentAttrs.viewBox == nil, let w = attr["width"]?.svg_asFloat, let h = attr["height"]?.svg_asFloat {
                    fragmentAttrs.viewBox = CGRect(x: 0, y: 0, width: w, height: h)
                }

                let pres = try assembledPresAttr(attr: attr)
                stack.append(.svg(id: id, fattr: fragmentAttrs, pattr: pres, kids: []))

            case "g":
                let pres = try assembledPresAttr(attr: attr)
                stack.append(.group(id: id, pattr: pres, kids: []))

            case "defs":
                stack.append(.defsWrapper)

            case "path":
                guard let d = attr["d"] else {
                    error = SVGSyntaxError(parser: parser, message: "missing required \"d\" attribute")
                    parser.abortParsing()
                    return
                }
                let path = try SVGPathReader().read(d)
                try pushFinishedElement(attr) { .graphic(id, $0, .path(path)) }

            case "rect":
                let rect = CGRect(x: attr["x"]?.svg_asFloat ?? 0,
                                  y: attr["y"]?.svg_asFloat ?? 0,
                                  width: try require("width", \.svg_asFloat),
                                  height: try require("height", \.svg_asFloat))
                let rx, ry: CGFloat?
                if let rxstr = attr["rx"], rxstr != "auto" {
                    rx = try require("rx", \.svg_asFloat)
                } else {
                    rx = nil
                }
                if let rystr = attr["ry"], rystr != "auto" {
                    ry = try require("ry", \.svg_asFloat)
                } else {
                    ry = nil
                }
                try pushFinishedElement(attr) { .graphic(id, $0, .rect(rect, rx: rx ?? ry ?? 0, ry: ry ?? rx ?? 0)) }

            case "circle":
                let cx = try require("cx", \.svg_asFloat)
                let cy = try require("cy", \.svg_asFloat)
                let r = try require("r", \.svg_asFloat)
                try pushFinishedElement(attr) { .graphic(id, $0, .circle(c: CGPoint(x: cx, y: cy), r: r)) }

            case "ellipse":
                let cx = try require("cx", \.svg_asFloat)
                let cy = try require("cy", \.svg_asFloat)
                let rx = try require("rx", \.svg_asFloat)
                let ry = try require("ry", \.svg_asFloat)
                try pushFinishedElement(attr) { .graphic(id, $0, .ellipse(c: CGPoint(x: cx, y: cy), rx: rx, ry: ry)) }

            case "line":
                let x1 = try require("x1", \.svg_asFloat)
                let y1 = try require("y1", \.svg_asFloat)
                let x2 = try require("x2", \.svg_asFloat)
                let y2 = try require("y2", \.svg_asFloat)
                try pushFinishedElement(attr) { .graphic(id, $0, .line(p1: CGPoint(x: x1, y: y1), p2: CGPoint(x: x2, y: y2))) }

            case "polyline":
                guard let coords = attr["points"]?.svg_asFloatList, coords.count % 2 == 0 else {
                    throw SVGSyntaxError(parser: parser, message: "polyline needs an even number of coordinates")
                }
                var points: [CGPoint] = []
                for i in stride(from: 0, to: coords.count, by: 2) {
                    points.append(CGPoint(x: coords[i], y: coords[i + 1]))
                }
                try pushFinishedElement(attr) { .graphic(id, $0, .polyline(points)) }

            case "polygon":
                guard let coords = attr["points"]?.svg_asFloatList, coords.count % 2 == 0 else {
                    throw SVGSyntaxError(parser: parser, message: "polygon needs an even number of coordinates")
                }
                var points: [CGPoint] = []
                for i in stride(from: 0, to: coords.count, by: 2) {
                    points.append(CGPoint(x: coords[i], y: coords[i + 1]))
                }
                try pushFinishedElement(attr) { .graphic(id, $0, .polygon(points)) }

            case "linearGradient":
                guard let id = id else {
                    throw SVGSyntaxError(parser: parser, message: "linearGradient requires an ID, so it can be referenced")
                }

                // gradient units and spread method are in here:
                let props = try assembledPresAttr(attr: attr)

                let gradient = StackItem.linearGradientDefinition(
                    id: id,
                    x1: attr["x1"]?.svg_asDistanceOrPercentage ?? .fraction(0),
                    y1: attr["y1"]?.svg_asDistanceOrPercentage ?? .fraction(0),
                    x2: attr["x2"]?.svg_asDistanceOrPercentage ?? .fraction(1),
                    y2: attr["y2"]?.svg_asDistanceOrPercentage ?? .fraction(0),
                    pattr: props,
                    stops: []
                )
                stack.append(gradient)

            case "stop":
                guard case .linearGradientDefinition(id: let id, x1: let x1, y1: let y1, x2: let x2, y2: let y2, pattr: let pattr, stops: var stops) = stack.last else {
                    throw SVGSyntaxError(parser: parser, message: "<stop> not inside a recognized gradient")
                }

                let offset = attr["offset"]?.svg_asFloatOrPercentage ?? 0

                let stop = try resovableWithPresAttr(attr) { pattr, lookup in
                    SVGGradientStop(
                        offset: offset,
                        color: pattr.get(SVGPAs.GradientStopColor.self, using: lookup),
                        opacity: pattr.get(SVGPAs.GradientStopOpacity.self, using: lookup)
                    )
                }

                stops.append(stop)
                stack.removeLast()
                stack.append(.linearGradientDefinition(id: id, x1: x1, y1: y1, x2: x2, y2: y2, pattr: pattr, stops: stops))

                // the stop, itself, has no content:
                stack.append(.skip)

            default:
                if reader.allowAndIgnoreUnsupportedSVG {
                    stack.append(.skip)
                    return
                }
                throw SVGSyntaxError(parser: parser, message: "unrecognized element name \"\(elementName)\"")
            }
        } catch {
            if failFast {
                self.error = error
                parser.abortParsing()
            } else {
                stack.append(.skip)
            }
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if case .style(var csstext) = stack.last {
            stack.removeLast()
            csstext += string
            stack.append(.style(csstext))
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        guard let stackTop = stack.last else {
            fatalError("bug: XML parser reported an end element for which I never pushed anything on the stack")
        }

        stack.removeLast()

        let finishedElement: SVGResolvable<SVGElement>
        switch stackTop {
        case .svg(let id, let frag, let pres, let kids):
            finishedElement = SVGResolvable { lookup in
                .svg(id, frag, .init(pres, using: lookup), kids.map { $0.resolve(using: lookup) })
            }

        case .group(let id, let pres, let kids):
            finishedElement = SVGResolvable { lookup in
                .group(id, .init(pres, using: lookup), kids.map { $0.resolve(using: lookup) })
            }

        case .finishedElement(let el):
            finishedElement = el

        case .skip, .defsWrapper:
            return // nothing to do here

        case .style(let css):
            do {
                definitions.register(try CSSReader().readRules(css, strict: failFast))
            } catch {
                if failFast {
                    self.error = error
                    parser.abortParsing()
                }
            }
            return

        case .linearGradientDefinition(id: let id, x1: let x1, y1: let y1, x2: let x2, y2: let y2, pattr: let pattr, stops: let stops):
            let lg = SVGResolvable { lookup in
                SVGPaint.linearGradient(
                    units: pattr.get(SVGPAs.GradientUnits.self, using: lookup),
                    transform: pattr.get(SVGPAs.Transform.self, using: lookup),
                    spread: pattr.get(SVGPAs.GradientSpreadMethod.self, using: lookup),
                    x1: x1, y1: y1, x2: x2, y2: y2,
                    stops: stops.map { $0.resolve(using: lookup) }
                )
            }
            definitions.register(id: id, lg)
            return
        }

        // we have a finished element; either it's the bottom of the stack, and
        // thus the root element, or there's a container beneath it, ready to
        // receive it
        //
        switch stack.last {
        case nil:
            finishedRootElement = finishedElement.resolve(using: definitions)

        case .svg(id: let id, fattr: let frag, pattr: let pres, kids: let kids):
            stack.removeLast()
            stack.append(.svg(id: id, fattr: frag, pattr: pres, kids: kids + [finishedElement]))

        case .group(id: let id, pattr: let pres, kids: let kids):
            stack.removeLast()
            stack.append(.group(id: id, pattr: pres, kids: kids + [finishedElement]))

        case .finishedElement, .style, .skip, .defsWrapper, .linearGradientDefinition:
            fatalError("bug: left a finished element on the stack, then processed another \(stack.last!)")
        }
    }
}
