//
//    String+SVGMicrosyntaxes.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation


internal extension String {
    var svg_asFloat: CGFloat? {
        return CGFloat((self as NSString).doubleValue)
    }

    /// Parses "10" as .distance(10), and "10%" as .fraction(0.1). It's up to
    /// the downstream consumer to decide what the meaning of these are.
    ///
    var svg_asDistanceOrPercentage: SVGDistanceOrPercentage? {
        if last == "%" {
            return .fraction(CGFloat((dropLast() as NSString).doubleValue / 100))
        }
        return .distance(CGFloat((self as NSString).doubleValue))
    }

    /// Parses "0.5" and "50%" both as 0.5. This is for properties where the
    /// value is always a fraction, but you can write it as a percentage.
    ///
    var svg_asFloatOrPercentage: CGFloat? {
        return svg_asDistanceOrPercentage?.absolute(percentageRelativeTo: 0...1)
    }

    var svg_asFloatList: [CGFloat]? {
        guard self != "none" else {
            return nil
        }
        return self
            .replacingOccurrences(of: ",", with: " ")
            .split(separator: " ")
            .map { CGFloat(($0 as NSString).doubleValue) }
    }

    var svg_asCSSClassList: [String] {
        return split(whereSeparator: \.isWhitespace).map(String.init(_:))
    }

    /// If the supplied string is of the form `url(#ID)`, returns ID. This
    /// is how SVG elements reference each other by ID in contexts which aren't
    /// specifically ID attributes.
    ///
    var svg_asReferenceToID: String? {
        guard starts(with: "url(#"), last == ")", count > 6 else {
            return nil
        }
        return String(dropFirst(5).dropLast())
    }
}
