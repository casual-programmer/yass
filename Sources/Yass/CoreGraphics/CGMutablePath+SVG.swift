//
//    CGMutablePath+SVG.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import CoreGraphics


extension CGMutablePath {
    /// Executes the given path instructions, appending to this path.
    ///
    public func svg_appendPath(_ instructions: [SVGPathInstruction]) {
        for instr in instructions {
            switch instr {
            case .moveTo(let pt):
                move(to: pt)
            case .closePath:
                closeSubpath()
            case .lineTo(let pt):
                addLine(to: pt)
            case .cubicCurveTo(let end, control1: let c1, control2: let c2):
                addCurve(to: end, control1: c1, control2: c2)
            case .arcTo(let end, radiusX: let rx, radiusY: let ry, rotation: let rot, largeArc: let largeArc, sweep: let sweep):
                svg_addArc(from: currentPoint, to: end, rx: rx, ry: ry, rotation: rot, largeArc: largeArc, sweep: sweep)
            case .quadCurveTo(let end, control: let cp):
                addQuadCurve(to: end, control: cp)
            }
        }
    }

    /// Adds the path for the given element to this path, including any of
    /// its specified transforms.
    ///
    public func svg_add(_ element: SVGElement) {
        switch element {
        case .graphic(_, let attrs, let graphic):
            guard attrs.display != .none else { break }
            let subpath = CGMutablePath()
            subpath.svg_add(graphic)
            addPath(subpath, transform: CGAffineTransform(svgTransforms: attrs.transform))

        case .group(_, let attrs, let elements),
             .svg(_, _, let attrs, let elements):
            guard attrs.display != .none else { break }
            let subpath = CGMutablePath()
            for element in elements {
                subpath.svg_add(element)
            }
            addPath(subpath, transform: CGAffineTransform(svgTransforms: attrs.transform))
        }
    }

    /// Adds the specified graphic to this path.
    ///
    public func svg_add(_ graphic: SVGGraphic) {
        switch graphic {
        case .path(let instrs):
            svg_appendPath(instrs)

        case .rect(let rect, rx: 0, ry: _),
             .rect(let rect, rx: _, ry: 0):
            // if either rx or ry are 0, it's a square corner; the math in the general case below could blow up
            addRect(rect)

        case .rect(let rect, rx: let rx, ry: let ry):
            let transform = CGAffineTransform(scaleX: 1.0, y: ry / rx)
            let path = CGMutablePath()
            path.addArc(center: CGPoint(x: rect.minX + rx, y: rect.minY + ry),
                        radius: rx,
                        startAngle: .pi, endAngle: .pi*3/2, clockwise: false,
                        transform: transform)
            path.addArc(center: CGPoint(x: rect.maxX - rx, y: rect.minY + ry),
                        radius: rx,
                        startAngle: .pi*3/2, endAngle: 0, clockwise: false,
                        transform: transform)
            path.addArc(center: CGPoint(x: rect.maxX - rx, y: rect.maxY - ry),
                        radius: rx,
                        startAngle: 0, endAngle: .pi/2, clockwise: false,
                        transform: transform)
            path.addArc(center: CGPoint(x: rect.minX + rx, y: rect.maxY - ry),
                        radius: rx,
                        startAngle: .pi/2, endAngle: .pi, clockwise: false,
                        transform: transform)
            path.closeSubpath()
            addPath(path)

        case .circle(c: let c, r: let r):
            addEllipse(in: CGRect(x: c.x - r, y: c.y - r,
                                  width: r * 2, height: r * 2))

        case .ellipse(c: let c, rx: let rx, ry: let ry):
            addEllipse(in: CGRect(x: c.x - rx, y: c.y - ry,
                                  width: rx * 2, height: ry * 2))

        case .line(p1: let p1, p2: let p2):
            move(to: p1)
            addLine(to: p2)

        case .polyline(let points):
            addLines(between: points)

        case .polygon(let points):
            addLines(between: points)
            closeSubpath()
        }
    }


    private func svg_addArc(from start: CGPoint, to end: CGPoint, rx: CGFloat, ry: CGFloat, rotation degrees: CGFloat, largeArc largeArcFlag: Bool, sweep sweepFlag: Bool, isRecursive: Bool = false) {

        // per SVG2 §9.5.1 "Out-of-range elliptical arc parameters":
        //
        if end == start {
            return
        }

        if rx == 0 || ry == 0 {
            addLine(to: end)
            return
        }

        let rx = abs(rx)
        let ry = abs(ry)

        // Get into radians:
        //
        let angle = CGFloat.pi * (degrees.truncatingRemainder(dividingBy: 360) / 180)

        // "Unit space" is transformed so that our ellipse is a unit circle around
        // some point (we don't know where yet). Everything starting with a `u`
        // below is in unit space.
        //
        let unitToDrawing = CGAffineTransform.identity
            .rotated(by: angle)
            .scaledBy(x: rx, y: ry)

        let drawingToUnit = unitToDrawing.inverted()

        let uStart = start.applying(drawingToUnit)
        let uEnd = end.applying(drawingToUnit)
        let uDist = hypot(uStart.x - uEnd.x, uStart.y - uEnd.y)

        if uDist == 0 {
            return // (it's the same point; should have been filtered out above anyway)
        } else if !isRecursive, uDist > 2 {
            // The points don't lie on the same unit circle in unit space. This means
            // they're too far apart for the given rx, ry, and angle. Per §9.5.1, we need
            // to uniformly scale up until they are; i.e., until they are exactly 2.0 apart
            // in unit space.
            //
            let factor = uDist / 2
            svg_addArc(from: start, to: end, rx: rx * factor, ry: ry * factor, rotation: degrees, largeArc: largeArcFlag, sweep: sweepFlag, isRecursive: true)
            return
        }

        let uMid = CGPoint(x: (uStart.x + uEnd.x) / 2, y: (uStart.y + uEnd.y) / 2)

        // We have two points which both lie on two circles (possibly one,
        // if they're 180º apart). We want the center of one of those
        // circles. `sign` picks which one. This is inverted because of
        // our flipped coordinate space (just like `clockwise`, below).
        //
        let sign: CGFloat
        switch (largeArcFlag, sweepFlag) {
        case (false, false), (true, true):  sign = 1    // on the RIGHT traveling start->end
        case (false, true), (true, false):  sign = -1   // on the LEFT traveling start->end
        }

        // Find the circle's center by solving the equivalent problem of intersecting
        // unit circles at uStart and uEnd. Circle-intersection math described (more
        // generally) at <http://paulbourke.net/geometry/circlesphere/>.
        //
        let uA = uDist / 2
        let uH = sqrt(1 - min(1, uA * uA)) // (floating-point error might make uA*uA slightly > 1, which breaks sqrt)
        let uCenter = CGPoint(x: uMid.x + sign * uH * (uEnd.y - uStart.y) / uDist,
                              y: uMid.y - sign * uH * (uEnd.x - uStart.x) / uDist)

        // Now we just need the angles in unit space.
        //
        let startAngle = atan2(uStart.y - uCenter.y, uStart.x - uCenter.x)
        let endAngle = atan2(uEnd.y - uCenter.y, uEnd.x - uCenter.x)

        // Add the arc, along with the transform back to drawing space.
        //
        addArc(center: uCenter, radius: 1, startAngle: startAngle, endAngle: endAngle, clockwise: !sweepFlag, transform: unitToDrawing)
    }
}
