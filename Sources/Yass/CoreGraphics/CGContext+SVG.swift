//
//    CGContext+SVG.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import CoreGraphics


private extension SVGDrawingAttributes {
    static let sRGB = CGColorSpace(name: CGColorSpace.sRGB)!

    func resolveColor(_ paint: SVGPaint, withOpacity opacity: CGFloat?) -> CGColor? {
        let alphaMul = opacity ?? 1
        switch paint {
        case .none:
            return nil

        case .rgba(let r, let g, let b, let a):
            return CGColor(colorSpace: Self.sRGB, components: [r, g, b, a * alphaMul])

        case .currentColor:
            guard currentColor != .currentColor else {
                return resolveColor(SVGPAs.CurrentColor.default, withOpacity: opacity)
            }
            return resolveColor(currentColor, withOpacity: opacity)

        case .linearGradient:
            return nil
        }
    }
}


extension CGContext {
    /// Draws the specified element, and any children it may have.
    ///
    /// The origin of `viewport` will be the origin of the drawing, but the drawing won't
    /// be automatically clipped to that viewport. (If the element is an `svg` element
    /// with a `view-box` attribute, a transform will be set up to map that box onto the
    /// given viewport.)
    ///
    /// If `withColor` is specified, that color will replace *all* colors (stroke or fill)
    /// within the drawing. Use this to easily put a single-color tint on an SVG asset.
    ///
    /// This saves and restores the context's graphics state.
    ///
    public func svg_draw(_ element: SVGElement,
                         in viewport: CGRect,
                         withColor color: CGColor? = nil) {
        saveGState()
        defer { restoreGState() }

        switch element {
        case .svg(_, let fragmentAttributes, let presentationAttributes, let contents):
            guard presentationAttributes.display != .none else { break }

            svg_applyTransforms(presentationAttributes.transform)

            translateBy(x: viewport.origin.x, y: viewport.origin.y)
            if let viewBox = fragmentAttributes.viewBox {
                let scale = min(viewport.size.width / viewBox.size.width, viewport.size.height / viewBox.size.height)
                scaleBy(x: scale, y: scale)
                translateBy(x: -viewBox.origin.x, y: -viewBox.origin.y)
            }

            for el in contents {
                svg_draw(el, in: fragmentAttributes.viewBox ?? viewport, withColor: color)
            }

        case .group(_, let presentationAttributes, let contents):
            guard presentationAttributes.display != .none else { break }

            svg_applyTransforms(presentationAttributes.transform)
            for el in contents {
                svg_draw(el, in: viewport, withColor: color)
            }

        case .graphic(_, let presentationAttributes, let graphic):
            guard presentationAttributes.display != .none else { break }

            svg_applyTransforms(presentationAttributes.transform)
            svg_buildPath(graphic)
            svg_drawCurrentPathWithAttributes(presentationAttributes, in: viewport, overridingColorsWith: color)
        }
    }

    /// Begins a new path and executes the given instructions to build it.
    /// (This clears the current path, if any.)
    ///
    /// Does not draw the path; call, for example,
    /// `svg_drawCurrentPathWithAttributes(_:)` to do so.
    ///
    public func svg_buildPath(_ instructions: [SVGPathInstruction]) {
        let path = CGMutablePath()
        path.svg_appendPath(instructions)

        beginPath()
        addPath(path)
    }

    /// Begins a new path and adds the specified graphic (shape or path) to
    /// it. This clears the current path, if any.
    ///
    /// Does not draw the path; call, for example,
    /// `svg_drawCurrentPathWithAttributes(_:)` to do so.
    ///
    public func svg_buildPath(_ graphic: SVGGraphic) {
        let path = CGMutablePath()
        path.svg_add(graphic)

        beginPath()
        addPath(path)
    }

    /// Updates the CTM with the given sequence of transforms.
    ///
    public func svg_applyTransforms(_ transforms: [SVGTransform]) {
        guard !transforms.isEmpty else {
            return
        }
        let m = CGAffineTransform(svgTransforms: transforms)
        if m != .identity {
            concatenate(m)
        }
    }

    @available(*, deprecated, message: "Use svg_drawCurrentPathWithAttributes(_:in:overridingColorsWith:) instead, providing the viewport")
    public func svg_drawCurrentPathWithAttributes(
        _ attr: SVGDrawingAttributes,
        overridingColorsWith overrideColor: CGColor? = nil
    ) {
        svg_drawCurrentPathWithAttributes(attr, in: boundingBoxOfClipPath, overridingColorsWith: overrideColor)
    }

    /// Strokes and/or fills the given path with the given attributes.
    ///
    /// This method strokes the path if `attr.stroke` is set to a valid paint,
    /// and fills it if `attr.fill` is set to a valid paint. It overwrites any
    /// existing stroke or fill properties in the current context, ignoring any
    /// previously-set values.
    ///
    /// Nils in the supplied attribute set will use default values per the SVG spec.
    ///
    /// If `overridingColorsWith` is specified, this will override any non-nil,
    /// non-`none` fill or stroke color.
    ///
    /// This method leaves the graphics state in unspecified state. If both stroke
    /// and fill are set, it will have modified many properties. It does not
    /// save/restore the state.
    ///
    /// Notes:
    /// - This clears the path
    /// - ... except that if both `fill` and `stroke` are `.none`, or if
    ///   `display` is `.none`, this is a big no-op (won't even clear the path)
    /// - Line joins of type `arcs` or `miterClip` are unsupported
    /// - For most numbers, I just let CoreGraphics validate or clamp their ranges
    ///
    public func svg_drawCurrentPathWithAttributes(
        _ attr: SVGDrawingAttributes,
        in viewport: CGRect,
        overridingColorsWith overrideColor: CGColor? = nil
    ) {
        guard attr.display != .none else { return }

        let fill = attr.fill
        let stroke = attr.stroke
        if fill == .none && stroke == .none {
            return
        }

        let fillRule = attr.fillRule

        var doStroke = false
        if let strokeColor = attr.resolveColor(stroke, withOpacity: attr.strokeOpacity) {
            doStroke = true
            setStrokeColor(overrideColor ?? strokeColor)
            configureStrokeProperties(from: attr)
        }

        var doFill = false
        if let fillColor = attr.resolveColor(fill, withOpacity: attr.fillOpacity) {
            doFill = true
            setFillColor(overrideColor ?? fillColor)
        } else if case .linearGradient(units: let units, transform: let transform, spread: let spread, x1: let x1, y1: let y1, x2: let x2, y2: let y2, stops: let stops) = fill {
            doFill = false
            fillPathWithLinearGradient(units: units, transform: transform, spread: spread, x1: x1, y1: y1, x2: x2, y2: y2, stops: stops, using: attr, in: viewport)
        }

        switch (doStroke, doFill, fillRule) {
        case (true, true, .nonzero):    drawPath(using: .fillStroke)
        case (true, true, .evenodd):    drawPath(using: .eoFillStroke)
        case (true, false, _):          drawPath(using: .stroke)
        case (false, true, .nonzero):   drawPath(using: .fill)
        case (false, true, .evenodd):   drawPath(using: .eoFill)
        case (false, false, _):         break   // should have short-circuited above
        }
    }

    private func fillPathWithLinearGradient(
        units: SVGGradientUnits,
        transform: [SVGTransform],
        spread: SVGGradientSpreadMethod,
        x1: SVGDistanceOrPercentage, y1: SVGDistanceOrPercentage,
        x2: SVGDistanceOrPercentage, y2: SVGDistanceOrPercentage,
        stops: [SVGGradientStop],
        using attr: SVGDrawingAttributes,
        in viewport: CGRect
    ) {
        saveGState()
        defer { restoreGState() }

        switch attr.fillRule {
        case .nonzero: clip(using: .winding)
        case .evenodd: clip(using: .evenOdd)
        }

        svg_applyTransforms(transform)

        let colors: [CGColor] = stops.map { stop in
            if let color = attr.resolveColor(stop.color, withOpacity: stop.opacity) {
                return color
            }
            return CGColor(gray: 0, alpha: stop.opacity)
        }
        var locations: [CGFloat] = stops.map { $0.offset }

        guard let gradient = CGGradient(
            colorsSpace: SVGDrawingAttributes.sRGB,
            colors: colors as CFArray,
            locations: &locations
        ) else {
            assert(false)
            return
        }

        let start, end: CGPoint
        switch units {
        case .objectBoundingBox:
            let box = boundingBoxOfClipPath

            // like they just could not stop themselves, they just couldn't
            // STAND it, every footnote has to have a footnote
            start = CGPoint(
                x: x1.butDistanceIsFraction.absolute(percentageRelativeTo: box.minX...box.maxX),
                y: y1.butDistanceIsFraction.absolute(percentageRelativeTo: box.minY...box.maxY)
            )
            end = CGPoint(
                x: x2.butDistanceIsFraction.absolute(percentageRelativeTo: box.minX...box.maxX),
                y: y2.butDistanceIsFraction.absolute(percentageRelativeTo: box.minY...box.maxY)
            )

        case .userSpaceOnUse:
            let box = viewport
            start = CGPoint(
                x: x1.absolute(percentageRelativeTo: box.minX...box.maxX),
                y: y1.absolute(percentageRelativeTo: box.minY...box.maxY)
            )
            end = CGPoint(
                x: x2.absolute(percentageRelativeTo: box.minX...box.maxX),
                y: y2.absolute(percentageRelativeTo: box.minY...box.maxY)
            )
        }

        drawLinearGradient(
            gradient,
            start: start,
            end: end,
            options: [.drawsBeforeStartLocation, .drawsAfterEndLocation]
        )
    }

    /// Configures everything about strokes except the paint and path, including
    /// join, cap, width, and miter limit, from the given attributes.
    ///
    private func configureStrokeProperties(from attr: SVGDrawingAttributes) {
        setLineDash(phase: attr.strokeDashOffset, lengths: attr.strokeDashArray)
        setLineWidth(attr.strokeWidth)

        switch attr.strokeLineCap {
        case .butt:     setLineCap(.butt)
        case .round:    setLineCap(.round)
        case .square:   setLineCap(.square)
        }

        switch attr.strokeLineJoin {
        case .miter, .miterClip, .arcs: setLineJoin(.miter)
        case .round:                    setLineJoin(.round)
        case .bevel:                    setLineJoin(.bevel)
        }

        setMiterLimit(attr.strokeMiterLimit)
    }
}
