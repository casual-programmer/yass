//
//    CGAffineTransform+SVG.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import CoreGraphics


extension CGAffineTransform {

    /// Constructs a new transform which represents the given SVG transform
    /// instructions, in the given sequence.
    ///
    public init(svgTransforms: [SVGTransform]?) {
        if let svgTransforms = svgTransforms {
            self = Self.identity.svg_concatenating(svgTransforms)
        } else {
            self = Self.identity
        }
    }

    /// Applies the given SVG-specified transforms, in sequence, to this one,
    /// returning the results.
    ///
    public func svg_concatenating(_ transforms: [SVGTransform]) -> Self {
        var m = self

        for t in transforms {
            switch t {
            case .rotate(let degrees, around: .zero):
                let radians = (degrees / 180.0) * .pi
                m = m.rotated(by: radians)
            case .rotate(let degrees, around: let point):
                let radians = (degrees / 180.0) * .pi
                m = m.translatedBy(x: point.x, y: point.y)
                     .rotated(by: radians)
                     .translatedBy(x: -point.x, y: -point.y)
            case .scale(let xscale, let yscale):
                m = m.scaledBy(x: xscale, y: yscale)
            case .translate(let dx, let dy):
                m = m.translatedBy(x: dx, y: dy)
            case .matrix(let a, let b, let c, let d, let tx, let ty):
                m = m.concatenating(.init(a: a, b: b, c: c, d: d, tx: tx, ty: ty))
            }
        }

        return m
    }

}
