//
//    SVGElements.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation
import CoreGraphics


/// Captures a single SVG element. (A loaded SVG document results in a single
/// root element of type `svg`.) This may be simplified or normalized, relative
/// to the actual XML parse tree, as SVG is full of options and shortcuts you
/// can use which don't bear on rendering.
///
/// The optional string included in each case is its element ID, if present.
/// (For elements in a `<defs>`, or elements like `linearGradient` which are
/// always treated as a definition regardless of parent, this ID is required,
/// and elements lacking it in the input are ignored.)
///
public enum SVGElement: Equatable {
    indirect case svg(String?, SVGFragmentAttributes, SVGDrawingAttributes, [SVGElement])
    indirect case group(String?, SVGDrawingAttributes, [SVGElement])
    case graphic(String?, SVGDrawingAttributes, SVGGraphic)
}


/// SVG's specific graphic types. Note that these only contain geometry;
/// style attributes and ID are attached to the enclosing `SVGElement.graphic`.
///
public enum SVGGraphic: Equatable {
    // Paths:
    case path([SVGPathInstruction])

    // Basic shapes:
    case rect(CGRect, rx: CGFloat, ry: CGFloat)
    case circle(c: CGPoint, r: CGFloat)
    case ellipse(c: CGPoint, rx: CGFloat, ry: CGFloat)
    case line(p1: CGPoint, p2: CGPoint)
    case polyline([CGPoint])
    case polygon([CGPoint])
}

/// Instructions for `SVGGraphic.path`.
///
/// All coordinates here are absolute. If the input SVG data contains relative
/// coordinates, or short-form curve types with implicit control points, the
/// actual absolute points are computed when the path is constructed.
///
public enum SVGPathInstruction: Equatable {
    case moveTo(CGPoint)
    case lineTo(CGPoint)
    case quadCurveTo(CGPoint, control: CGPoint)
    case cubicCurveTo(CGPoint, control1: CGPoint, control2: CGPoint)
    case arcTo(CGPoint, radiusX: CGFloat, radiusY: CGFloat, rotation: CGFloat, largeArc: Bool, sweep: Bool)
    case closePath
}

/// Fragment attributes (on the `svg` element).
///
public struct SVGFragmentAttributes: Equatable {
    public var viewBox: CGRect?
}


public extension SVGElement {
    /// Extracts a sub-element by ID.
    ///
    func findID(_ id: String) -> SVGElement? {
        switch self {
        case .svg(.some(id), _, _, _),
             .group(.some(id), _, _),
             .graphic(.some(id), _, _):
            return self

        case .svg(_, _, _, let contents),
             .group(_, _, let contents):
            return contents.lazy.compactMap { $0.findID(id) }.first

        default:
            return nil
        }
    }
}
