//
//    SVGStyling.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation
import CoreGraphics


/// The common SVG presentational attributes which apply to all drawn elements:
/// paths, shapes, and their containers.
///
/// These can be declared on the XML element itself, inherited from parent
/// elements, or applied by inline or stylesheet CSS, and all have spec-defined
/// defaults. Note that some SVG elements might have different, or additional,
/// presentational attributes, if they don't represent drawn paths.
///
public struct SVGDrawingAttributes: Equatable {
    // General properties:
    public var display: SVGDisplay

    // Fill-related styling:
    public var fill: SVGPaint
    public var fillRule: SVGFillRule
    public var fillOpacity: CGFloat

    // Stroke-related styling:
    public var stroke: SVGPaint
    public var strokeOpacity: CGFloat
    public var strokeWidth: CGFloat
    public var strokeLineCap: SVGLineCap
    public var strokeLineJoin: SVGLineJoin
    public var strokeMiterLimit: CGFloat
    public var strokeDashArray: [CGFloat]
    public var strokeDashOffset: CGFloat

    // Accumulated transforms:
    public var transform: [SVGTransform]

    /// This contains the value of the `color` property. It's not directly used
    /// when rendering; rather, it provides a value which is referenced when
    /// a fill or stroke color is set to `.currentColor`.
    ///
    /// (If this itself is set to `.currentColor`, it is treated as if nil.
    /// That's not allowed by the CSS syntax.)
    ///
    public var currentColor: SVGPaint
}

/// In SVG, the CSS `display` property may be set to `none` to remove a
/// subtree completely from rendering; any value other than `none` causes it
/// to be rendered as normal. The default value is `inline`; per spec, any
/// value other than `none` is treated identically to `inline`, so I don't
/// preserve the original value if it wasn't that (e.g. if it were inherited
/// from some other CSS rule).
///
public enum SVGDisplay: Equatable & Sendable {
    case none
    case inline
}

/// A "paint", which can be used to fill or stroke a shape. This may be a
/// color, a gradient, or "none", to disable filling/stroking.
///
public enum SVGPaint: Equatable & Sendable {
    case none
    case rgba(CGFloat, CGFloat, CGFloat, CGFloat)
    case currentColor
    case linearGradient(
        units: SVGGradientUnits,
        transform: [SVGTransform],
        spread: SVGGradientSpreadMethod,
        x1: SVGDistanceOrPercentage,
        y1: SVGDistanceOrPercentage,
        x2: SVGDistanceOrPercentage,
        y2: SVGDistanceOrPercentage,
        stops: [SVGGradientStop]
    )

    public static let black = Self.rgba(0, 0, 0, 1)

    // unsupported: hsla(), context-fill, context-stroke
}

/// A single logical transform step; usually placed in an array which
/// concatenates these transforms from left to right. The library reports
/// these as separate steps, rather than as a premultiplied matrix, to make
/// it easier to inspect and debug.
///
public enum SVGTransform: Equatable & Sendable {
    case translate(CGFloat, CGFloat)
    case rotate(CGFloat, around: CGPoint)
    case scale(CGFloat, CGFloat)
    case matrix(CGFloat, CGFloat, CGFloat, CGFloat, CGFloat, CGFloat)
    // not supported: skew[XY]
    // not supported: explicit units (px, for example)
}

/// The fill rule used when filling a path with a paint.
///
public enum SVGFillRule: String, Sendable {
    case nonzero        = "nonzero"
    case evenodd        = "evenodd"
}

/// The line-end style when stroking an open path.
///
public enum SVGLineCap: String, Sendable {
    case butt           = "butt"
    case round          = "round"
    case square         = "square"
}

/// The line-join style when stroking a path with multiple segments.
///
public enum SVGLineJoin: String, Sendable {
    case miter          = "miter"
    case miterClip      = "miter-clip"
    case round          = "round"
    case bevel          = "bevel"
    case arcs           = "arcs"
}

/// How SVG interprets the coordinates which define a gradient, including
/// both percentage-formatted and number-formatted values.
///
public enum SVGGradientUnits: String, Sendable {
    case userSpaceOnUse     = "userSpaceOnUse"
    case objectBoundingBox  = "objectBoundingBox"
}

/// How the space beyond the "ends" of a gradient is filled.
///
/// Yass does not, at present, implement this. Everything is treated as `pad`.
///
public enum SVGGradientSpreadMethod: String, Sendable {
    case pad            = "pad"
    case reflect        = "reflect"
    case `repeat`       = "repeat"
}

/// A stop along an SVG gradient.
///
public struct SVGGradientStop: Equatable & Sendable {
    /// The offset, where 0 and 1 represent the start and end of a linear
    /// gradient's vector, or the center and outermost circle of a radial
    /// gradient.
    ///
    var offset: CGFloat

    /// The SVG color attached to this point.
    ///
    var color: SVGPaint

    /// An opacity multiplier for `color`.
    ///
    var opacity: CGFloat
}

/// Either an absolute value (of unspecified units) or a percentage (of an
/// unspecified range). This is deliberately vague, because proper
/// interpretation often depends on context and other attributes.
///
public enum SVGDistanceOrPercentage: Equatable & Sendable {
    case distance(CGFloat)  // absolute value
    case fraction(CGFloat)  // between 0 and 1

    public var butDistanceIsFraction: Self {
        switch self {
        case .distance(let n), .fraction(let n): return .fraction(n)
        }
    }

    public func absolute(percentageRelativeTo range: ClosedRange<CGFloat>) -> CGFloat {
        switch self {
        case .distance(let n):  return n
        case .fraction(let n):  return range.lowerBound + n * (range.upperBound - range.lowerBound)
        }
    }
}
