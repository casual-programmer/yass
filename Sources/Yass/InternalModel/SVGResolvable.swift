//
//    SVGResolvable.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation


/// A context in which defined IDs (e.g., from `<defs>`) and CSS stylesheet
/// rules (e.g., from `<style>`) may be looked up. `SVGReader` builds one of
/// these as it goes, then uses it at the end to resolve all cross references.
///
struct SVGLookup {
    func lookup<T>(id: String, ofType type: T.Type) -> T? {
        if let val = resolvableValues[id] as? SVGResolvable<T> {
            return val.resolve(using: self)
        } else {
            return nil
        }
    }

    func lookupCSS(elementID: String?, elementClasses: [String]) -> SVGPresentationAttributeSet {
        return cssRules
            .filter { $0.selector.matches(id: elementID, classes: elementClasses) }
            .map { $0.presentationAttributes }
            .reduce(into: .init()) { $0.merge($1) }
    }

    mutating func register(_ cssRules: [CSSRule]) {
        self.cssRules.append(contentsOf: cssRules)
    }

    mutating func register<T>(id: String, _ resolvable: SVGResolvable<T>) {
        resolvableValues[id] = resolvable
    }

    private var resolvableValues: [String: Any] = [:]
    private var cssRules: [CSSRule] = []
}


/// Nothing but the deferred computation of an SVG object; the reader "resolves"
/// all of these against its final set of definitions before returning them to
/// the client.
///
struct SVGResolvable<T> {
    var resolver: (_ lookup: SVGLookup) -> T

    func resolve(using lookup: SVGLookup) -> T {
        resolver(lookup)
    }

    static func fixed(_ value: T) -> Self {
        return Self { _ in value }
    }
}
