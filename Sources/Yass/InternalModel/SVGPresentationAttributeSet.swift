//
//    SVGPresentationAttributeSet.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation


/// A strongly-typed dictionary of presentation attributes, encapsulating
/// direct values, indirect lookups (`url(#id)`), and CSS lookups.
///
/// This is not exposed to the client; once SVG loading is finished, we will
/// resolve everything we can, then collapse to appropriate concrete structures
/// in the public model.
///
struct SVGPresentationAttributeSet {
    init(elementID: String? = nil, elementClasses: [String] = []) {
        self.elementID = elementID
        self.elementClasses = elementClasses
    }

    func get<Attr: SVGPresentationAttribute>(_ attr: Attr.Type, using lookup: SVGLookup) -> Attr.Value {
        let key = key(for: attr)
        let css = lookup.lookupCSS(elementID: elementID, elementClasses: elementClasses)

        // Per SVG spec, CSS-supplied presentation attributes take priority.
        switch css.assignments[key] ?? assignments[key] {
        case nil:
            return attr.default

        case .value(let val):
            return val as! Attr.Value

        case .idref(let id):
            if let found = lookup.lookup(id: id, ofType: Attr.Value.self) {
                return found
            }
            return attr.default
        }
    }

    mutating func set<Attr: SVGPresentationAttribute>(value: Attr.Value, for attr: Attr.Type) {
        assignments[key(for: attr)] = .value(value)
    }

    mutating func set(value: Any, for attr: any SVGPresentationAttribute.Type) {
        assignments[key(for: attr)] = .value(attr.eraseValueType(value))
    }

    mutating func set<Attr: SVGPresentationAttribute>(refTo id: String, for attr: Attr.Type) {
        assignments[key(for: attr)] = .idref(id)
    }

    mutating func merge(_ other: Self) {
        for (key, assignment) in other.assignments {
            assignments[key] = assignment
        }
    }

    mutating func inherit(from parent: Self) {
        for attr in SVGPAs.all where attr.inherits {
            let key = key(for: attr)
            if assignments[key] == nil {
                assignments[key] = parent.assignments[key]
            }
        }
    }

    func merging(_ other: Self) -> Self {
        var copy = self
        copy.merge(other)
        return copy
    }

    private let elementID: String?
    private let elementClasses: [String]
    private var assignments: [AttrKey: Assignment] = [:]

    private enum Assignment {
        case value(any Equatable)
        case idref(String)
    }

    private typealias AttrKey = ObjectIdentifier
    private func key<Attr: SVGPresentationAttribute>(for attr: Attr.Type) -> AttrKey {
        ObjectIdentifier(attr)
    }

    private static func attributeType(forKey key: AnyHashable) -> (any SVGPresentationAttribute.Type)? {
        return SVGPAs.all.first { AnyHashable(ObjectIdentifier($0)) == key }
    }
}

/// Equatability is currently only used in unit tests, and the semantics are
/// "exactly equal", not, for example, "semantically equivalent".
///
extension SVGPresentationAttributeSet: Equatable {
    static func == (a: Self, b: Self) -> Bool {
        guard
            a.elementID == b.elementID,
            a.elementClasses == b.elementClasses,
            a.assignments.keys == b.assignments.keys
        else {
            return false
        }

        for key in a.assignments.keys {
            guard let t = attributeType(forKey: key) else {
                assert(false) // bug
                return false
            }

            switch (a.assignments[key], b.assignments[key]) {
            case (.value(let aa), .value(let bb)):
                if !t.compareTypeErasedAttributeValues(aa, bb) {
                    return false
                }

            case (.idref(let aa), .idref(let bb)):
                if aa != bb {
                    return false
                }

            default:
                return false
            }
        }

        return true
    }
}


