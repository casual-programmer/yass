//
//    SVGPAs.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation


/// Namespace containing all the SVGPresentationAttribute types; "SVGPA"
/// is just "SVG Presentation Attribute".
///
enum SVGPAs {
    /// All presentation attributes defined by this version of the library,
    /// including both common and element-specific ones.
    ///
    static let all: [any SVGPresentationAttribute.Type] = [
        Display.self,
        Fill.self,
        FillOpacity.self,
        FillRule.self,
        Stroke.self,
        StrokeWidth.self,
        StrokeOpacity.self,
        StrokeLineCap.self,
        StrokeLineJoin.self,
        StrokeMiterLimit.self,
        StrokeDashArray.self,
        StrokeDashOffset.self,
        GradientUnits.self,
        GradientSpreadMethod.self,
        GradientStopColor.self,
        GradientStopOpacity.self,
        Transform.self,
        CurrentColor.self,
    ]

    struct Display: SVGPresentationAttribute {
        static let name = "display"
        static let `default` = SVGDisplay.inline
        static func read(_ str: String) -> SVGDisplay? { str.lowercased() == "none" ? SVGDisplay.none : .inline }
    }

    struct Fill: SVGPresentationAttribute {
        static let name = "fill"
        static let `default` = SVGPaint.black
        static func read(_ str: String) -> SVGPaint? { SVGPaintReader().read(str) }
    }

    struct FillOpacity: SVGPresentationAttribute {
        static let name = "fill-opacity"
        static let `default`: CGFloat = 1.0
        static func read(_ str: String) -> CGFloat? { str.svg_asFloatOrPercentage }
    }

    struct FillRule: SVGPresentationAttribute {
        static let name = "fill-rule"
        static let `default` = SVGFillRule.nonzero
        static func read(_ str: String) -> SVGFillRule? { SVGFillRule(rawValue: str) }
    }

    struct Stroke: SVGPresentationAttribute {
        static let name = "stroke"
        static let `default` = SVGPaint.none
        static func read(_ str: String) -> SVGPaint? { SVGPaintReader().read(str) }
    }

    struct StrokeWidth: SVGPresentationAttribute {
        static let name = "stroke-width"
        static let `default`: CGFloat = 1.0
        static func read(_ str: String) -> CGFloat? { str.svg_asFloat }
    }

    struct StrokeOpacity: SVGPresentationAttribute {
        static let name = "stroke-opacity"
        static let `default`: CGFloat = 1.0
        static func read(_ str: String) -> CGFloat? { str.svg_asFloatOrPercentage }
    }

    struct StrokeLineCap: SVGPresentationAttribute {
        static let name = "stroke-linecap"
        static let `default` = SVGLineCap.butt
        static func read(_ str: String) -> SVGLineCap? { SVGLineCap(rawValue: str) }
    }

    struct StrokeLineJoin: SVGPresentationAttribute {
        static let name = "stroke-linejoin"
        static let `default` = SVGLineJoin.miter
        static func read(_ str: String) -> SVGLineJoin? { SVGLineJoin(rawValue: str) }
    }

    struct StrokeMiterLimit: SVGPresentationAttribute {
        static let name = "stroke-miterlimit"
        static let `default`: CGFloat = 4.0
        static func read(_ str: String) -> CGFloat? { str.svg_asFloat }
    }

    struct StrokeDashArray: SVGPresentationAttribute {
        static let name = "stroke-dasharray"
        static let `default`: [CGFloat] = []
        static func read(_ str: String) -> [CGFloat]? { str.svg_asFloatList }
    }

    struct StrokeDashOffset: SVGPresentationAttribute {
        static let name = "stroke-dashoffset"
        static let `default`: CGFloat = 0
        static func read(_ str: String) -> CGFloat? { str.svg_asFloat }
    }

    struct GradientUnits: SVGPresentationAttribute {
        static let name = "gradientUnits"
        static let `default` = SVGGradientUnits.objectBoundingBox
        static func read(_ str: String) -> SVGGradientUnits? { SVGGradientUnits(rawValue: str) }
    }

    struct GradientSpreadMethod: SVGPresentationAttribute {
        static let name = "spreadMethod"
        static let `default` = SVGGradientSpreadMethod.pad
        static func read(_ str: String) -> SVGGradientSpreadMethod? { SVGGradientSpreadMethod(rawValue: str) }
    }

    struct GradientStopColor: SVGPresentationAttribute {
        static let name = "stop-color"
        static let `default` = SVGPaint.black
        static func read(_ str: String) -> SVGPaint? { SVGPaintReader().read(str) }
    }

    struct GradientStopOpacity: SVGPresentationAttribute {
        static let name = "stop-opacity"
        static let `default`: CGFloat = 1.0
        static func read(_ str: String) -> CGFloat? { str.svg_asFloatOrPercentage }
    }

    struct Transform: SVGPresentationAttribute {
        static let name = "transform"
        static let altNames = ["gradientTransform", "patternTransform"]
        static let `default`: [SVGTransform] = []
        static let inherits = false
        static func read(_ str: String) throws -> [SVGTransform]? { try SVGTransformReader().read(str) }
    }

    struct CurrentColor: SVGPresentationAttribute {
        static let name = "color"
        static let `default` = SVGPaint.black
        static func read(_ str: String) -> SVGPaint? { SVGPaintReader().read(str) }
    }
}
