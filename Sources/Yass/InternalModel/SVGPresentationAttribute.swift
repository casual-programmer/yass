//
//    SVGPresentationAttribute.swift, part of the Yass library (https://gitlab.com/casual-programmer/yass)
//    Copyright 2020-2022 Aaron Trickey (Casual Programmer, LLC)
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in all
//    copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//    SOFTWARE.
//

import Foundation


/// "Presentation attributes" define the styling of SVG elements, and can be
/// specified directly on the elements or indirectly via CSS rules or inline
/// CSS styles.
///
protocol SVGPresentationAttribute {
    associatedtype Value: Equatable

    static var name: String { get }
    static var altNames: [String] { get }
    static var `default`: Value { get }
    static var inherits: Bool { get }

    static func read(_ str: String) throws -> Value?
}

extension SVGPresentationAttribute {
    static var altNames: [String] { [] }
    static var inherits: Bool { true }
}

extension SVGPresentationAttribute {
    static func compareTypeErasedAttributeValues(_ a: Any, _ b: Any) -> Bool {
        guard let a = a as? Value, let b = b as? Value else {
            return false
        }
        return a == b
    }

    static func eraseValueType(_ value: Any) -> any Equatable {
        if let value = value as? Value {
            return value
        }
        assert(false)
        return `default`
    }
}

